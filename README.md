# Genomics England HTSGet
This repo houses Genomics England's implementation of the GA4GH HTSGet streaming protocol.

HTSGet is a protocol used in conjunction with a data retrieval API to stream sections of genomic files. The name HTSGet comes from HTSlib, the library that underpins the bcftools and samtools program suites.

It is not uncommon to see read alignment files reaching >100GB in size, which makes them effectively impossible to view on a personal computer. By only retrieving the parts of the file of interest at any one time, HTSGet alleviates this issue. This also has the knock-on effect of increasing security (removing unnecessary data transfer) and reducing file transfer errors (decreasing the possibility that a file is not properly downloaded).

The GEL HTSGet implementation is specialised for connecting to and authenticating against OpenCGA, hence the reason we maintain our own deployment and can't use an off-the-shelf protocol (similarly, the DNAnexus HTSGet implementation is customised to use their own database).

For a brief overview of HTSGet functionality, see [this](https://www.youtube.com/watch?v=yWCVl8MJ6P4&feature=emb_title) YouTube video. Further technical detail can be obtained by Genomics England employees by visiting [this link](https://cnfl.extge.co.uk/display/BTS/HTSGet) or by speaking to a member of KMDS.  For out-of-hours support help, see [here](https://cnfl.extge.co.uk/display/KMDS/HTSGet+Support).