#!/bin/sh

grep '^ *"htsget_version":' $1 | awk '{ print $2 }' | sed -e 's/,$//' -e 's/^"//' -e 's/"$//' >> $2
