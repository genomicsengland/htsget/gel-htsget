#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import logging

from tornado import web


class HtsGetHttpError(web.HTTPError):
    def __init__(self, *args, status_code=500, log_message=None, **kwargs):
        logging.error(kwargs["reason"])
        super(HtsGetHttpError, self).__init__(status_code, log_message, *args, **kwargs)
        self.error = kwargs.get("error", "UnhandledException")


class HtsGetInvalidUrlError(web.HTTPError):
    def __init__(self, *args, status_code=400, log_message=None, **kwargs):
        logging.error(kwargs["reason"])
        super(HtsGetInvalidUrlError, self).__init__(
            status_code, log_message, *args, **kwargs
        )
        self.error = kwargs.get("error", "UnhandledException")


class HtsGetInvalidUrlWarning(web.HTTPError):
    def __init__(self, *args, status_code=400, log_message=None, **kwargs):
        logging.warning(kwargs["reason"])
        super(HtsGetInvalidUrlWarning, self).__init__(
            status_code, log_message, *args, **kwargs
        )
        self.error = kwargs.get("error", "UnhandledException")
