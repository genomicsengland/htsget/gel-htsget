#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import logging.config
import os
import sys

from cachetools import TTLCache

# Must be done before importing tornado as per https://ddtrace.readthedocs.io/en/stable/integrations.html#tornado
from ddtrace import tracer, patch

patch(tornado=True, logging=True)

from ddtrace.filters import FilterRequestsOnUrl
from tornado import ioloop, web
from tornado.options import options, define
from tornado_swagger_ui import get_tornado_handler

from handlers.data_handler import DataHandler
from handlers.health_handler import HealthHandler, PingHandler
from handlers.reads_handler import ReadsHandler
from handlers.variants_handler import VariantsHandler
from htsget_server.settings import Settings, logging_conf


# Configure Datadog tracer for logs
tracer.configure(
    hostname=Settings.get("DD_AGENT_HOST"),
    settings={
        "FILTERS": [
            FilterRequestsOnUrl([r"https://htsget(.*)/ping(.*)$"]),
        ],
    },
)


define("port", default=8888, help="run on the given port", type=int)
logging.config.dictConfig(logging_conf())


def flush_cache():
    return {
        "user_cache": TTLCache(
            maxsize=Settings.get("USER_CACHE_SIZE"), ttl=Settings.get("USER_CACHE_TTL")
        ),
        "header_cache": TTLCache(
            maxsize=Settings.get("HEADER_CACHE_SIZE"),
            ttl=Settings.get("HEADER_CACHE_TTL"),
        ),
    }


"""
cachetools allows for the use of TTL-cache - "time to live". this uses the in-built functionality to have the contents
expire. cachetools also includes decorator wraps so methods can be wrapped by default (e.g. each time we call method X,
see if the result was already cached)
BUT
the cache has to be something that can be created here, and passed to multiple handler instances
by default the wrapping only covers re-called methods from a single instance

This could be done with a custom decorator to save more lines of code, but this is currently a proof of concept to
see how/IF it improves performance
"""
cache_dictionary = flush_cache()


class Htsget:
    """"""

    def __init__(self, application_settings):
        """"""
        global cache_dictionary
        if application_settings.get("REFRESH_CACHE"):
            cache_dictionary = flush_cache()

        self.application_settings = application_settings
        swagger_handlers = get_tornado_handler(
            base_url="/api/docs",
            api_url="/swagger.yaml",
            config={"app_name": "gel-htsget"},
        )

        handlers = [
            (r"/ping/", PingHandler),
            (r"/health/1/?(.+)", HealthHandler),
            (r"/health/1/", HealthHandler),
            (r"/reads/(.+)", ReadsHandler, {"cache": cache_dictionary}),
            (r"/variants/(.+)", VariantsHandler, {"cache": cache_dictionary}),
            (r"/data", DataHandler, {"cache": cache_dictionary}),
            (
                r"/(swagger\.yaml)",
                web.StaticFileHandler,
                {"path": os.path.join(os.path.dirname(os.path.abspath(__file__)))},
            ),
        ]
        handlers.extend(swagger_handlers)

        self.application = web.Application(handlers=handlers, **Settings)

    @staticmethod
    def start():
        """"""
        ioloop.IOLoop.instance().start()

    @staticmethod
    def stop():
        """"""
        ioloop.IOLoop.instance().stop()


def main():
    """"""
    options.parse_command_line()
    htsget = Htsget(application_settings=Settings)
    htsget.application.listen(options.port)
    htsget.start()


if __name__ == "__main__":
    sys.exit(main())
