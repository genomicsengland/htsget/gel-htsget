openapi: 3.0.0
components:
  schemas:
    /headers:
      title: headers
      type: object
      properties:
        Authorization:
          type: string
        Range:
          type: string
    /urls:
      title: urls
      required:
      type: object
      properties:
        url:
          type: string
        headers:
          type: object
          $ref: '#/components/schemas/~1headers'
      example:
        url: https://server/reads|variants/study/sample?format=x
    /htsget_response:
      title: htsgetResponse
      properties:
        format:
          type: string
        urls:
          type: array
          items:
            type: object
            $ref: '#/components/schemas/~1urls'
    /htsget_error:
      title: htsget_error
      properties:
        reason:
          type: string
        message:
          type: string
        error_code:
          type: integer
    /reads_request:
      title: read_request
      optional:
      - reference
      - start
      - end
      - format
      type: object
      properties:
        reference:
          type: string
        format:
          type: string
        start:
          type: integer
        end:
          type: integer
      example:
        reference: chr1
        start: 100000
        end: 2000000
        format: BAM
    /variants_request:
      title: variant_request
      optional:
      - reference
      - start
      - end
      - format
      type: object
      properties:
        reference:
          type: string
        format:
          type: string
        start:
          type: integer
        end:
          type: integer
      example:
        reference: chr1
        start: 100000
        end: 2000000
        format: VCF
    HealthCheckResponse:
      type: object
      required:
        - status
        - serviceName
        - datetime
      properties:
        serviceName:
          type: string
          description: 'Name of the service'
        requestUrl:
          type: string
        datetime:
          type: string
          format: 'date-time'
          description: 'date time when check was performed'
        depenpendencies:
          $ref: '#/components/schemas/HealthCheckDependencies'
        status:
          $ref: '#/components/schemas/Status'
        components:
          type: array
          description: 'Complete list of components of this application'
          items:
            type: string
        unavailableComponents:
          description: 'List of components within this service that are not available. This list should appear when the status is Degraded, Down or Not Configured'
          type: array
          items:
            type: string
    HealthCheckDependencies:
      type: object
      properties:
        datastores:
          type: array
          items:
            $ref: '#/components/schemas/HealthCheckDependency'
        apis:
          type: array
          items:
            $ref: '#/components/schemas/HealthCheckDependency'
    HealthCheckDependency:
      type: object
      required:
        - url
        - status
      properties:
        type:
          type: string
          description: 'Type of dependency. For example: `REST API`'
        description:
          type: string
          description: 'Description of the dependency. For example: `Exomiser`'
        url:
          type: string
          description: 'Url of the service'
        status:
          $ref: '#/components/schemas/Status'
        additionalProperties:
          type: object
    Status:
      type: string
      enum: ['DOWN', 'DEGRADED', 'NOT_CONFIGURED', 'OK']
  securitySchemes:
    httpBearer:
      type: http
      scheme: bearer
info:
  title: gel-htsget
  description: |
    # Introduction

    gel-htsget is an API to stream parts of genomic files. It is implemented  using the GA4GH htsget protocol v1.2.0.

    # Overview

    Authentication is done against OpenCGA. Currently this can be done using LDAP, AD or internal OpenCGA credentials. Metadata on genomic data e.g, associated studies,samples etc. is held in OpenCGA. This allows the identification of  the relevant file.

    # Error Codes


    | Error | Associated Code |
    | ------- | -------- |
    | Invalid Authentication   | 401    |
    | Permission Denied   | 403    |
    | Not found   | 404    |
    | Unsupported Format   | 400    |
    | Invalid Input   | 400    |
    | Invalid Range   | 400    |


  contact: {}
  version: "1.0.0"
servers:
# Added by API Auto Mocking Plugin
- description: Current
  url: /
  variables: {}
paths:
  /ping/1/:
    get:
      summary: Return the status of the service, including the dependencies
      responses:
        200:
          description: OK
        503:
          description: Service Unavailable
  /health/1/:
    get:
      summary: Return the status of the service, including the dependencies
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/HealthCheckResponse'
        503:
          description: Service Unavailable
      parameters:
        - name: skip
          description: Comma separated list of services names that will be skip from the check
          in: query
          schema:
            type: string
        - name: token
          description: API token for health check. When passed all of the dependencies and their status will be displayed. The dependencies will be checked if this parameter is not used, but they won't be part of the response.
          in: query
          schema:
            type: string
  /reads/{id}:
    get:
      tags:
      - Misc
      summary: /reads
      description: This endpoint is specific to alignment files (Bam/CRAM). It returns an array of URLs pointed at the /data endpoint. This endpoint requires an OpenCGA recognised token to authenticate. Study and sample are passed in as part of the URL, e.g /reads/RD38GMS/LP_XXXX
      operationId: /reads
      parameters:
        - name: id
          in: path
          schema:
            type: string
        - name: format
          in: query
          description: BAM or CRAM
          required: false
          style: form
          explode: true
          schema:
            type: string
        - name: referenceName
          in: query
          description: Chromosome or Locus Name
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: start
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: end
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
      responses:
        200:
          description: ''
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_response'
        401:
          description: 'InvalidAuthentication'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'

        400:
          description: 'Invalid Request: Range or Format'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'
        504:
          description: 'Upstream server has failed'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'

      deprecated: false
  /data:
    get:
      tags:
      - Misc
      summary: /data
      description: Provides actual data (Reads or Variants)
      operationId: /data
      parameters:
        - name: study
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: fileId
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: format
          in: query
          description: BAM, CRAM, VCF
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: referenceName
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: start
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: end
          in: query
          required: true
          style: form
          explode: true
          schema:
            type: string
        - name: class
          in: query
          required: false
          style: form
          explode: true
          schema:
            type: string
      responses:
        200:
          description: ''
          headers: {}
          content:
            text/plain:
              schema:
                type: object
        401:
          description: 'Invalid Authentication'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'

      deprecated: false
  /variants/{id}:
    get:
      tags:
      - Misc
      summary: /variants
      description: This endpoint is specific to variant files (VCF). It returns an array of URLs pointed at the /data endpoint.)  This endpoint requires an OpenCGA recognised token to authenticate.Study, Variant type and sample are passed in as part of the URL, e.g /variants/CS38/SMALL_VARIANT/LP_XXXXX
      operationId: /variants
      parameters:
        - name: id
          in: path
          schema:
            type: string
        - name: referenceName
          in: query
          required: false
          style: form
          explode: true
          schema:
            type: string
        - name: start
          in: query
          required: false
          style: form
          explode: true
          schema:
            type: string
        - name: end
          in: query
          required: false
          style: form
          explode: true
          schema:
            type: string
      responses:
        200:
          description: ''
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_response'
        401:
          description: 'Invalid Authentication'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'
        400:
          description: 'Invalid Request: Range or Format'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'
        504:
          description: 'Upstream server has failed'
          headers: {}
          content:
            text/plain:
              schema:
                type: object
                $ref: '#/components/schemas/~1htsget_error'

      deprecated: false


security:
- httpBearer: []
tags:
- name: Misc
  description: ''