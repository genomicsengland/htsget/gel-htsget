#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import os

from decouple import Config, RepositoryEnv, AutoConfig

settings_location = os.getenv("SETTINGS_LOCATION")
if settings_location is not None:
    config = Config(RepositoryEnv(settings_location))
else:
    config = AutoConfig()

Settings = dict(
    REFRESH_CACHE=config(
        "REFRESH_CACHE", default=False, cast=bool
    ),  # mainly used in test cases - delete cache
    USER_CACHE_SIZE=config("USER_CACHE_SIZE", default=100, cast=int),
    USER_CACHE_TTL=config("USER_CACHE_TTL", default=8000, cast=int),
    HEADER_CACHE_SIZE=config("HEADER_CACHE_SIZE", default=20, cast=int),
    HEADER_CACHE_TTL=config("HEADER_CACHE_TTL", default=3600, cast=int),
    SUBREGION_HEADER_LABEL=config("SUBREGION_HEADER_LABEL", default="X-Subregion"),
    HEADER_LENGTH_LABEL=config("HEADER_LENGTH_LABEL", default="Header-bytes-length"),
    READS_FORMAT=("BAM", "CRAM"),
    VARIANTS_FORMAT=("VCF",),
    VALID_CLASSES=("header",),
    REF_TYPES=("fa", "fasta"),
    BAM_FIELDS=(
        "QNAME",
        "FLAG",
        "RNAME",
        "POS",
        "MAPQ",
        "CIGAR",
        "RNEXT",
        "PNEXT",
        "TLEN",
        "SEQ",
        "QUAL",
    ),
    INT_FILTERS=("mapq",),
    FLOAT_FILTERS=("downsample",),
    STRING_FILTERS=(
        "quality",
        "samples",
    ),
    SAMTOOLS_EXTPATH=config("SAMTOOLS_EXPATH", default="samtools"),
    BCFTOOLS_EXTPATH=config("BCFTOOLS_EXPATH", default="bcftools"),
    GENOMES_MOUNT_PATHS=config(
        "GENOMES_MOUNT_PATHS",
        default="/not/a/real/path/",
        cast=lambda v: [s for s in v.split(",") if s != ""],
    ),
    SUBPROCESS_TIMEOUT=config("SUBPROCESS_TIMEOUT", default=40, cast=int),
    RANGE_LIMIT=config("RANGE_LIMIT", default=10000000, cast=int),
    HEALTH_CHECK_TIMEOUT=config("HEALTH_CHECK_TIMEOUT", default=3, cast=int),
    HEALTH_CHECK_CACHE_SECONDS=config(
        "HEALTH_CHECK_CACHE_SECONDS", default=10, cast=int
    ),
    SUBREGION_CHUNK_SIZE=config("SUBREGION_CHUNK_SIZE", default=100000, cast=int),
    CHUNK_SIZE_IN_BYTES=config("CHUNK_SIZE_IN_BYTES", default=20000000, cast=int),
    opencga_host=config("OPENCGA_HOST", default="not-the-real-host"),
    opencga_version=config("OPENCGA_VERSION", default="not-the-real-version"),
    opencga_file_search_options=config("OPENCGA_FILE_SEARCH_OPTIONS", default="{}"),
    opencga_vcf_types=config(
        "OPENCGA_VCF_TYPES", default="", cast=lambda v: [s for s in v.split(",")]
    ),
    HEALTH_CHECKS=config("HEALTH_CHECKS", default=("opencga,mount")).split(
        ","
    ),  # GEL apps
    HEALTH_CHECK_CRITICAL=config(
        "HEALTH_CHECK_CRITICAL", default=("opencga,mount")
    ).split(
        ","
    ),  # GEL apps
    HEALTH_CHECK_SERVICE_NAME=config("HEALTH_CHECK_SERVICE_NAME", default="htsget"),
    HEALTH_CHECK_TOKEN=config("HEALTH_CHECK_TOKEN", "htsget-health-token"),
    downsample_rate=config("downsample_rate", default=0.1),
    DD_AGENT_HOST=config("DD_AGENT_HOST", "localhost"),
    LOG_LEVEL=config("LOG_LEVEL", "INFO"),
    GLOBAL_RETRIES=config("GLOBAL_RETRIES", default=0, cast=int),
    GLOBAL_BASE_BACKOFF=config("GLOBAL_BASE_BACKOFF", default=1, cast=int),
    GLOBAL_MAX_BACKOFF=config("GLOBAL_MAX_BACKOFF", default=30, cast=int),
)

LOG_FORMATTER_VALUES = [
    "asctime",
    "filename",
    "levelname",
    "lineno",
    "module",
    "message",
    "name",
    "pathname",
    "dd.trace_id",
    "dd.span_id",
]

LOG_FORMAT = " ".join(["%({0:s})".format(name) for name in LOG_FORMATTER_VALUES])


def logging_conf():
    return {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "json": {
                "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
                "fmt": LOG_FORMAT,
                "datefmt": "%Y-%m-%dT%H:%M:%SZ",
            },
        },
        "filters": {},
        "handlers": {
            "console": {
                "level": Settings.get("LOG_LEVEL"),
                "class": "logging.StreamHandler",
                "formatter": "json",
            },
        },
        "loggers": {
            "": {
                "handlers": ["console"],
                "level": Settings.get("LOG_LEVEL"),
            },
        },
    }
