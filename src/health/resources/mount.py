import logging
import os
from time import time

from health.enums import Status, ResourceType
from health.resources.base import Resource
from htsget_server.settings import Settings

LOGGER = logging.getLogger(__name__)


class GenomeMount(Resource):
    def __init__(self):
        self.name = "File System Mount"
        self.connection_type = "OTHER"
        self.resource_type = ResourceType.DATASTORE

        if Settings["GENOMES_MOUNT_PATHS"]:
            self.is_configured = True
            self.description = "File System Mount is OK"
        else:
            self.description = "File System Not Configured"

        super(GenomeMount, self).__init__(filename=__file__)

    def check(self, skip_resources=None, disable_cache=False):
        if (
            not disable_cache
            and self.last_checked
            and time() - self.last_checked < Settings["HEALTH_CHECK_CACHE_SECONDS"]
        ):
            LOGGER.debug(
                "Not running health check for resource {}, using cached data".format(
                    self.name
                )
            )
            return self.status

        fail_status = (
            Status.DOWN
            if (self.resource_name in getattr(Settings, "HEALTH_CHECK_CRITICAL", []))
            else Status.DEGRADED
        )

        if self.is_configured:
            status = Status.OK

            for path in Settings["GENOMES_MOUNT_PATHS"]:
                if not os.path.exists(path):
                    status = fail_status
                    self.description = path + " is not mounted"

                elif len(os.listdir(path)) <= 0:
                    status = fail_status
                    self.description = path + " is empty"

        else:
            status = Status.NOT_CONFIGURED

        self.status = status
        return self.status


resource = GenomeMount
