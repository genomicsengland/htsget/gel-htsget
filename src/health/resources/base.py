import logging
import os
from typing import List

import requests
from time import time

from health.enums import Status, ResourceType
from htsget_server.settings import Settings

LOGGER = logging.getLogger(__name__)


class Resource(object):
    """Abstract class for the resource"""

    name = ""
    resource_name = ""
    description = ""
    can_skip = False
    last_checked = None
    is_configured = False
    resource_type = None
    failed_status = Status.DOWN
    url = None
    health_path = ""
    # level = ERROR
    connection_type = None
    status = None
    check_time = None
    affected: List = list()  # list of affected parts of code (user facing)

    def __init__(self, filename=__file__):
        if not self.is_configured:
            self.status = Status.NOT_CONFIGURED
        else:
            self.validate_setup()
        self.resource_name = os.path.basename(filename).replace(".py", "").lower()

    def __str__(self):  # py3
        return "[Health] {}({}) Status: {}".format(
            self.name, self.connection_type, self.status.value
        )

    def __unicode__(self):
        return "[Health] {}({}) Status: {}".format(
            self.name, self.connection_type, self.status.value
        )

    def to_dict(self, *args, **kwargs):
        # TODO (Oleg) this isn't strictly compatible with GelReportModels
        #    use two different classes for API and Datastore and override `to_dict`

        additional_properties = {}
        res = {
            "status": self.status.value,
            "type": self.connection_type,
            "description": self.description,
            "name": self.name,
            "url": [self.safe_url] if self.url else [],
        }

        if self.health_path:
            additional_properties["healthPath"] = self.health_path

        if not self.ok and self.affected:
            additional_properties["affected"] = ",".join(self.affected)

        if additional_properties:
            res["additionalProperties"] = additional_properties

        return res

    @property
    def ok(self):
        return self.status == Status.OK

    def validate_setup(self):
        """Check if child classes are configured"""

        assert self.name
        assert self.resource_type
        # assert self.url
        # assert self.level
        assert self.connection_type

    def check(self, skip_resources=None, disable_cache=False):
        raise NotImplementedError()

    @property
    def safe_url(self):
        """Return URL without auth part"""

        if self.url:
            return self.url.split("@")[1] if "@" in self.url else self.url


class SimpleHTTPResource(Resource):
    """Abstract REST Resource check"""

    failed_status = Status.DOWN
    health_path = ""  # relative path to the health endpoint
    method = "GET"
    resource_type = ResourceType.API

    def __init__(self, *args, **kwargs):
        self.connection_type = "REST"
        super(SimpleHTTPResource, self).__init__(*args, **kwargs)

    @property
    def headers(self):
        return {
            "HTTP_USER_AGENT": "gel_health_checks service:{}".format(
                Settings["HEALTH_CHECK_SERVICE_NAME"]
            )
        }

    @property
    def request_params(self):
        return {}

    def check(self, skip_resources=None, disable_cache=False):
        if not self.is_configured:
            # Don't run check if it's not configured
            LOGGER.warning(
                "{} resource is not configured for health checks".format(self.name)
            )
            return

        if (
            not disable_cache
            and self.last_checked
            and time() - self.last_checked < Settings["HEALTH_CHECK_CACHE_SECONDS"]
        ):
            LOGGER.debug(
                "Not running health check for resource {}, using cached data".format(
                    self.name
                )
            )
            return

        if self.resource_name in getattr(Settings, "HEALTH_CHECK_CRITICAL", []):
            self.failed_status = Status.DOWN

        query_params = ""

        if self.can_skip:
            query_params = "?skip="
            if "?" in self.health_path:
                query_params = "&skip="
            query_params += ",".join(skip_resources) if skip_resources else ""

            if disable_cache is True:
                query_params += "&disable_cache=true"

        _status = self.failed_status
        try:
            url = "{}{}{}".format(self.url, self.health_path, query_params)
            response = requests.request(
                self.method,
                url,
                headers=self.headers,
                timeout=Settings["HEALTH_CHECK_TIMEOUT"],
                **self.request_params
            )
            if response.ok:
                _status = Status.OK
            if post_check_status := self.post_check(response):
                _status = post_check_status
        except Exception as error:
            LOGGER.error(error)

        self.status = _status
        self.last_checked = time()

        return self.status

    def post_check(self, response):
        """Check the response body and return new status

        :param response: `requests` Response object
        :return: Status value or None
        """

        return None
