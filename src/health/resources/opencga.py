from typing import Optional

from requests import Response

from health.enums import Status
from health.resources.base import SimpleHTTPResource
from htsget_server.settings import Settings


class OpenCGA(SimpleHTTPResource):
    def __init__(self, url: Optional[str] = None, health_path: Optional[str] = None):
        self.name = "OpenCGA"
        self.affected = ["authentication", "accessing files"]
        self.is_configured = True
        if url is None:
            self.url = Settings["opencga_host"]
        else:
            self.url = url

        if health_path is None:
            self.health_path = "/webservices/rest/{version}/meta/health".format(
                version=Settings["opencga_version"]
            )
        else:
            self.health_path = health_path

        super(OpenCGA, self).__init__(filename=__file__)

    def post_check(self, response: Response) -> Optional[Status]:
        # FIXME: including 503 as a workaround for https://jira.extge.co.uk/browse/KMDS-1853
        if response.status_code in [200, 503]:
            return Status(response.json()["status"])
        return None


resource = OpenCGA
