import datetime
import logging
from importlib import import_module
from typing import Optional

from health.enums import ResourceType, Status
from health.resources.base import Resource
from htsget_server.settings import Settings

LOGGER = logging.getLogger(__name__)


class HealthChecks(object):
    @staticmethod
    def get_module(name):
        resource = None

        try:
            package_name = "health.resources.{}".format(name)
            resource_class = import_module(package_name).resource
            if issubclass(resource_class, Resource):
                resource = resource_class()
            else:
                resource = None
        except ImportError as e:
            LOGGER.error(e)

        return resource

    def __init__(
            self,
            now=None,
            resources: Optional[list[Resource]] = None,
    ):
        if now is None:
            self.now = datetime.datetime.now
        else:
            self.now = now
        if resources is None:
            self.resources = []
            self.last_checked = None
            modules = {}  # to avoid duplicates
            for name in Settings["HEALTH_CHECKS"]:
                resource = self.get_module(name)
                if resource:
                    modules[resource.__class__.__name__] = resource

            self.resources = list(modules.values())
        else:
            self.resources = resources

    def run(self, skip_resources=None, disable_cache=False):
        """Run all checks (at the moment sequentially) and update their states

        TODO when this is moved to Py3 implement parallel execution as all
             of them can be run independently.
        """
        for res in self.resources:
            if res.resource_name not in skip_resources:
                res.check(skip_resources=skip_resources, disable_cache=disable_cache)

    def to_dict(
            self,
            request_url,
            show_dependencies=False,
            skip_resources=None,
            disable_cache=False,
    ):
        """Convert to Serializer compatible object (for APIv2)"""
        skip_resources = skip_resources or []
        self.run(skip_resources, disable_cache)

        global_status = Status.OK

        dependencies = {"apis": [], "datastores": []}
        unavailable_components = []
        component_names = []

        for resource in self.resources:
            component_names.append(resource.name)
            if resource.resource_name in skip_resources:
                continue
            resource_type = resource.resource_type
            if resource_type == ResourceType.API:
                dependencies["apis"].append(resource.to_dict())
            elif resource_type == ResourceType.DATASTORE:
                dependencies["datastores"].append(resource.to_dict())

            if resource.status is not Status.OK:
                unavailable_components.append(resource.name)

            if resource.status < global_status:
                global_status = resource.status

        data = {
            "status": global_status.value,
            "datetime": self.now().strftime("%Y-%m-%dT%H:%M:%SZ%Z"),
            "requestUrl": request_url,
            "serviceName": Settings["HEALTH_CHECK_SERVICE_NAME"],
            "components": component_names,
        }

        if global_status in [Status.DOWN, Status.DEGRADED, Status.NOT_CONFIGURED]:
            data["unavailableComponents"] = unavailable_components

        if show_dependencies:
            data["dependencies"] = dependencies

        return data


health_checks = HealthChecks()
