#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from abc import ABC

import tornado.gen
import tornado.web
import json

from health.enums import Status
from health.health_checks import health_checks
from htsget_server.settings import Settings


class PingHandler(tornado.web.RequestHandler, ABC):
    """"""

    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        pong = {"ping": "pong"}
        self.write(json.dumps(pong))


class HealthHandler(tornado.web.RequestHandler, ABC):
    """"""

    def head(self, *args, **kwargs):
        self.get(args, kwargs)

    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        # checking dependencies (opencga) connection and file system mount
        data = health_checks.to_dict(
            self.request.full_url(),
            show_dependencies=self.display_extra,
            skip_resources=self.skip_resources,
            disable_cache=self.disable_cache,
        )
        if data["status"] == Status.DOWN.value:
            self.set_status(503)
        else:
            self.set_status(200)

        self.write(json.dumps(data))
        self.finish()

    @property
    def display_extra(self):
        token = self.get_arguments("token")[0] if "token" in self.request.query else ""
        return token and token == self.settings["HEALTH_CHECK_TOKEN"]

    @property
    def skip_resources(self):
        skip = self.get_arguments("skip")[0] if "skip" in self.request.query else ""
        skip_resources = skip.lower().replace("%20", " ").split(",") if skip else []

        # Avoid loops by always skipping the service requesting the health check
        skip_resources.append(Settings["HEALTH_CHECK_SERVICE_NAME"].lower())
        return set(skip_resources)

    @property
    def disable_cache(self):
        disable_cache = (
            self.get_arguments("disable_cache")[0]
            if "disable_cache" in self.request.query
            else ""
        )
        return disable_cache and disable_cache.lower() == "true"
