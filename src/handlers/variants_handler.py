#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from functools import partial
import logging
from tenacity import (
    before_sleep_log,
    retry,
    stop_after_attempt,
    wait_random_exponential,
    retry_if_not_exception_type,
)

import tornado.gen
from tornado.ioloop import IOLoop

from handlers.base_handler import BaseHandler, thread_pool_executor
from htsget_server.settings import Settings
from htsget_errors.htsget_http_error import (
    HtsGetInvalidUrlWarning,
    HtsGetInvalidUrlError,
)


class VariantsHandler(BaseHandler):
    """"""

    @retry(
        reraise=True,
        stop=stop_after_attempt(Settings["GLOBAL_RETRIES"]),
        wait=wait_random_exponential(
            multiplier=Settings["GLOBAL_BASE_BACKOFF"],
            max=Settings["GLOBAL_MAX_BACKOFF"],
        ),
        before_sleep=before_sleep_log(logging.getLogger(), logging.WARNING),
        retry=retry_if_not_exception_type(
            (HtsGetInvalidUrlWarning, HtsGetInvalidUrlError)
        ),
    )
    @tornado.gen.coroutine
    def get(self, file_id):
        args = self._get_args("variants")
        session = yield IOLoop.current().run_in_executor(
            thread_pool_executor,
            partial(
                self.authentication_manager_class.login,
                token=self.request.headers.get("Authorization", ""),
                **self.settings
            ),
        )

        cache_key = "{}::{}".format(session.token, file_id)
        opencga_content = self.cache["user_cache"].get(cache_key)
        if not opencga_content:
            opencga_content = yield IOLoop.current().run_in_executor(
                thread_pool_executor,
                partial(session.get_file, file_id=file_id, data_type="variants"),
            )
            self.cache["user_cache"][cache_key] = opencga_content

        # Creating the response URL to retrieve data
        response = self._create_response(
            args, self.request.host, opencga_content, session.token
        )
        self.write(response)

    def head(self, *args, **kwargs):
        self.finish()
