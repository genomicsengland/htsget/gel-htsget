#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from abc import ABC
import json
import logging
from urllib.parse import quote

from tornado import web

from htsget_auth.opencga_authentication_manager import OpencgaAuthenticationManager
from htsget_errors.htsget_http_error import (
    HtsGetHttpError,
    HtsGetInvalidUrlWarning,
)
from utilities.utilities import Utilities


from concurrent.futures import ThreadPoolExecutor

# Set limit to 2000 to prevent ulimit being reached
thread_pool_executor = ThreadPoolExecutor(2000)

HTS_INDEX_EXTENSIONS = (".tbi", ".csi", ".bai", ".crai")


class BaseHandler(web.RequestHandler, ABC):
    """"""

    authentication_manager_class = OpencgaAuthenticationManager

    def initialize(self, cache):
        """
        provides access to the central cache dictionary for all subclasses of the RequestHandler
        """
        self.cache = cache

    def options(self, *args, **kwargs):
        method = self.request.headers.get("Access-Control-Request-Method", "")
        if method and method == "GET":
            self.set_header("Access-Control-Max-Age", 2592000)
            headers = self.request.headers.get("Access-Control-Request-Headers", "")
            if headers:
                self.set_header("Access-Control-Allow-Headers", headers)
        self.set_status(204)
        self.finish()

    def set_default_headers(self):
        self.set_header(
            "Access-Control-Allow-Headers",
            "access-control-allow-origin,authorization,content-type,x-requested-with",
        )
        self.set_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header(
            "Content-Type", "application/vnd.ga4gh.htsget.v1.1.1+json; charset=utf-8"
        )

    def write_error(self, status_code, **kwargs):
        error = ""
        msg = ""
        if self._reason.__contains__("::"):
            try:
                error, msg = self._reason.split("::")
                logging.info("{} {}".format(error, msg))
            except Exception as exception:
                logging.error(exception)
                error, msg = self._reason, self._reason
        else:
            if "exc_info" in kwargs:
                for item in kwargs["exc_info"]:
                    if isinstance(item, Exception):
                        error = "Error"
                        msg = str(item)

        self.finish(json.dumps({"htsget": {"error": error, "message": msg}}))

    def _get_args(self, endpoint):
        assert endpoint in ["reads", "variants", "data"]

        args = {
            arg: self.request.arguments[arg][0].decode("utf-8")
            for arg in self.request.arguments
        }

        # check for spurious index file requests (from samtools) and fail early
        for idx_ext in HTS_INDEX_EXTENSIONS:
            if self.request.uri.endswith(idx_ext) or self.request.path.endswith(
                idx_ext
            ):
                raise HtsGetInvalidUrlWarning(
                    reason="File extension is not supported",
                    status_code=400,
                    error="NotFound",
                )

        ftype = args.get("format", None)
        ref = args.get("referenceName", None)
        start = args.get("start", None)
        end = args.get("end", None)
        data_class = args.get("class", None)

        try:
            if start is not None:
                int(start)
            if end is not None:
                int(end)
        except ValueError:
            raise HtsGetHttpError(
                reason="One of the start or end attributes is not an int ({start}, {end})".format(
                    start=start, end=end
                ),
                status_code=400,
                error="InvalidInput",
            )

        if (
            endpoint == "variants" or ftype in self.settings["VARIANTS_FORMAT"]
        ) and ref == "*":
            raise HtsGetHttpError(
                reason="referenceName * is invalid for variant requests",
                status_code=400,
                error="NotFound",
            )

        # class argument restrictions apply for reads and variants
        if data_class and not endpoint == "data":
            if data_class not in self.settings.get("VALID_CLASSES"):
                raise HtsGetHttpError(
                    reason="Requested class is not supported",
                    status_code=400,
                    error="InvalidInput",
                )
            if any(
                {start, end, ref}
            ):  # spec requires failure message if class + other variables (format allowed)
                raise HtsGetHttpError(
                    reason="Class argument not valid with other parameters",
                    status_code=400,
                    error="InvalidInput",
                )

        # UnsupportedFormat error if the requested format is not supported
        if not ftype:
            args["format"] = (
                "CRAM" if endpoint == "reads" else "VCF"
            )  # BAM is the specification default
        else:
            if (
                endpoint == "reads" and ftype not in self.settings.get("READS_FORMAT")
            ) or (
                endpoint == "variants"
                and ftype not in self.settings.get("VARIANTS_FORMAT")
            ):
                raise HtsGetHttpError(
                    reason="Requested format is not supported",
                    status_code=400,
                    error="UnsupportedFormat",
                )

        # InvalidInput error if start but no reference or referenceName is "*"
        if (start or end) and not ref and endpoint != "data":
            raise HtsGetHttpError(
                reason="Coordinates are specified and either no reference is specified or referenceName is *",
                status_code=400,
                error="InvalidInput",
            )

        # InvalidRange error if start is greater than end
        if start and end and int(start) > int(end):
            raise HtsGetHttpError(
                reason="Start greater than end", status_code=400, error="InvalidRange"
            )

        return args

    @staticmethod
    def _get_coordinates(args, query_file):
        """
        :param args: dictionary parsed from URL arguments
        :param query_file: File, created in OpencgaAuthenticationManager._convert_opencga_file
        """

        reference = Utilities.check_chromosome_prefix_in_reference(
            args.get("referenceName", None), query_file.contigs
        )
        start = int(args["start"]) if "start" in args else None
        end = int(args["end"]) if "end" in args else None

        return reference, start, end

    def _create_response(
        self,
        url_args,
        host,
        query_file,
        token,
        max_region_size=100000,
        header_length=0,
    ):
        data_class = url_args.get("class", None)
        ref, start, end = self._get_coordinates(url_args, query_file)

        # subset of fields using flags only works with CRAM?
        # fields = url_args.get("fields", None)
        #
        # if fields:
        #     failing_flags = Utilities.validate_samflags(fields, self.settings['BAM_FIELDS'])
        #     if failing_flags:
        #         raise HtsGetHttpError(
        #             reason="Requested fields {} are not valid SAMFlags".format(failing_flags),
        #             status_code=400,
        #             error="InputError"
        #         )

        filters = url_args.get("filters", None)

        if filters:
            Utilities.parse_url_filters(filters, self.settings, check_only=True)

        if data_class == "header" and any([ref, start, end]):
            raise HtsGetHttpError(
                reason="Positional parameters provided with class=header",
                status_code=400,
                error="InvalidInput",
            )

        Utilities.validate_coordinates(ref, start, end)

        file_format = url_args["format"]
        file_id = query_file.response_id

        response_object = {"htsget": {"format": file_format, "urls": []}}

        url_base = "https://{host}/data?format={format}&{file_id}".format(
            host=host, format=file_format, file_id=file_id
        )

        header_url = "{}{}".format(url_base, "&class=header")
        if filters:
            header_url += "&filters={}".format(quote(filters))

        response_object["htsget"]["urls"].append(
            {
                "url": header_url,
                "headers": {"Authorization": token},
                "class": "header",
            }
        )

        if data_class == "header":
            response_object["htsget"]["urls"][-1]["headers"]["Eof"] = "true"
            return response_object

        if file_format in self.settings.get("VARIANTS_FORMAT"):
            url = "{}&class=body".format(url_base)
            if any([ref, start, end]):
                url += "{}".format(
                    Utilities.fill_out_coordinate_string(ref, start, end, query_file)
                )

            if filters:
                url += "&filters={}".format(quote(filters))

            response_object["htsget"]["urls"].append(
                {
                    "url": url,
                    "headers": {"Authorization": token, "Eof": "true"},
                    "class": "body",
                }
            )
            return response_object

        if file_format in self.settings["READS_FORMAT"]:
            # First url is a header only request. Subsequent URLS are body requests - split into chunks
            htsget_region_dict = Utilities.htsget_regions_dict(
                ref,
                start,
                end,
                query_file.contigs,
                query_file.contig_length,
                max_region_size,
            )

            for chromosome in htsget_region_dict:
                if chromosome == "*":
                    header = {
                        "Authorization": token,
                        "Header-bytes-length": str(header_length),
                        self.settings["SUBREGION_HEADER_LABEL"]: "*",
                    }

                    url = "{}&class=body&referenceName=*".format(url_base)

                    # if fields:
                    #     url = "{}&fields={}".format(url, fields)

                    if filters:
                        url = "{}&filters={}".format(url, quote(filters))

                    response_object["htsget"]["urls"].append(
                        {"url": url, "headers": header, "class": "body"}
                    )
                    break

                for start_coordinate, end_coordinate in htsget_region_dict[chromosome]:
                    header = {
                        "Authorization": token,
                        self.settings["SUBREGION_HEADER_LABEL"]: "{}:{}-{}".format(
                            chromosome, start_coordinate, end_coordinate
                        ),
                        "Header-bytes-length": str(header_length),
                    }
                    url = "{base}&class=body{region}".format(
                        base=url_base,
                        region=Utilities.fill_out_coordinate_string(
                            chromosome, start_coordinate, end_coordinate, query_file
                        ),
                    )

                    # if fields:
                    #     url = "{}&fields={}".format(url, fields)

                    if filters:
                        url = "{}&filters={}".format(url, quote(filters))

                    response_object["htsget"]["urls"].append(
                        {"url": url, "headers": header, "class": "body"}
                    )

            response_object["htsget"]["urls"][-1]["headers"]["Eof"] = "true"

            return response_object

        raise HtsGetHttpError(
            reason="Invalid request type; must be Variants or Reads",
            status_code=400,
            error="InvalidInput",
        )
