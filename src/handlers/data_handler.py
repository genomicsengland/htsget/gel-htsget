#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import io
import logging
import os
import re
import subprocess
from functools import partial

from tenacity import (
    before_sleep_log,
    retry,
    stop_after_attempt,
    wait_random_exponential,
    retry_if_not_exception_type,
)
import tornado.gen
from tornado.ioloop import IOLoop

from handlers.base_handler import BaseHandler, thread_pool_executor
from htsget_errors.htsget_http_error import (
    HtsGetHttpError,
    HtsGetInvalidUrlWarning,
    HtsGetInvalidUrlError,
)
from utilities import filter_aligment_by_end_position_pysam
from htsget_server.settings import Settings
from utilities.utilities import Utilities


class DataHandler(BaseHandler):
    """"""

    def _get_header_data_response(self, eof, eof_length, cmdline_args):
        main_out, _ = self._get_subprocess_out_and_stderr(cmd_line_args=cmdline_args)

        if eof is not None:
            return main_out
        return main_out[0:-eof_length]

    @staticmethod
    def _identify_bcftools_error_category(error_blob):
        """
        Mini method for finding failure modes of bcftools
        """
        error_decoded = error_blob.decode("utf-8")
        if "Could not retrieve index file for" in error_decoded:
            raise HtsGetHttpError(
                reason="BCFTools failed - no index present for requested file",
                status_code=400,
                error="InvalidRequest",
            )
        else:
            raise HtsGetHttpError(
                reason="Unknown BCFTools error: {}".format(error_decoded),
                status_code=400,
                error="InvalidRequest",
            )

    def _get_body_data_response(
        self, bam_compression_cmd_process, eof, eof_length, header_length
    ):
        result = bam_compression_cmd_process.communicate(
            timeout=self.settings["SUBPROCESS_TIMEOUT"]
        )

        out = result[0]
        if eof is not None:
            return out[int(header_length) :]
        return out[int(header_length) : -eof_length]

    @staticmethod
    def _yield_chunks(datastream, chunk_size):
        """
        :param datastream: io.BytesIO object implementing .read() method to obtain chunks
        :param chunk_size: number of Bytes per chunk to obtain
        """

        while True:
            vcf_buffer = datastream.read(chunk_size)
            if not vcf_buffer:
                break
            yield vcf_buffer
        datastream.close()

    def validate_region(self, region):
        if region == "*":
            return

        try:
            chromosome, coordinates = region.split(":")
            start, end = map(int, coordinates.split("-"))

        except ValueError:
            raise HtsGetHttpError(
                reason="Please provide a valid {} value: `contig:start-end`".format(
                    self.settings["SUBREGION_HEADER_LABEL"]
                ),
                status_code=400,
                error="InvalidRequest",
            )

        Utilities.validate_coordinates(
            chromosome, start, end, chunk_size=self.settings["SUBREGION_CHUNK_SIZE"]
        )

    def _get_data_response(
        self,
        format,
        file_uri,
        region,
        is_header,
        filter_dict,
        fields=None,
        header_length=None,
        eof=None,
    ):
        try:
            eof_length = 28
            if format == "CRAM":
                eof_length = 12

            main_cmd_array = self._generate_samtools_main_command(
                file_uri, is_header, region, filter_dict
            )

            if is_header:
                header_key = "header_content::{}".format(file_uri)
                header_content = self.cache["header_cache"].get(header_key)

                if header_content is None:
                    header_content = self._get_header_data_response(
                        eof, eof_length, main_cmd_array
                    )
                    self.cache["header_cache"][header_key] = header_content

                return header_content

            elif region == "*":
                main_out = subprocess.Popen(main_cmd_array, stdout=subprocess.PIPE)
                return self._get_body_data_response(
                    main_out, eof, eof_length, header_length
                )

            bam_compression_out = self._build_full_samtools_pipeline(
                region, eof, main_cmd_array
            )
            return self._get_body_data_response(
                bam_compression_out, eof, eof_length, header_length
            )

        except subprocess.TimeoutExpired:
            logging.error(
                msg="samtools timed out on {}; Timeout is currently set to {}s".format(
                    file_uri, self.settings["SUBPROCESS_TIMEOUT"]
                )
            )
            raise HtsGetHttpError(
                log_message="Timeout",
                reason="Request has timed out on file {}".format(file_uri),
                status_code=400,
                error="NotFound",
            )

    def _build_full_samtools_pipeline(self, region, eof, main_cmd_array):
        filter_script_path = os.path.abspath(
            filter_aligment_by_end_position_pysam.__file__
        )
        filter_cmd_array = ["python3", filter_script_path, region.split("-")[-1]]

        # if fields:
        #     fields_as_flag = Utilities.translate_field_to_filter_flag(fields)
        #     bam_compression_array = [self.settings["SAMTOOLS_EXTPATH"], "view", *fields_as_flag, "-b", "-1"]

        bam_compression_array = [self.settings["SAMTOOLS_EXTPATH"], "view", "-b", "-1"]
        main_cmd_process = subprocess.Popen(main_cmd_array, stdout=subprocess.PIPE)

        if eof is None:
            logging.info(
                "{} | {} | {}".format(
                    " ".join(main_cmd_array),
                    " ".join(filter_cmd_array),
                    " ".join(bam_compression_array),
                )
            )
            filter_cmd_process = subprocess.Popen(
                filter_cmd_array,
                stdin=main_cmd_process.stdout,
                stdout=subprocess.PIPE,
            )

            bam_compression_cmd_process = subprocess.Popen(
                bam_compression_array,
                stdin=filter_cmd_process.stdout,
                stdout=subprocess.PIPE,
            )
            main_cmd_process.stdout.close()
            filter_cmd_process.stdout.close()

        else:
            logging.info(
                "{} | {}".format(
                    " ".join(main_cmd_array), " ".join(bam_compression_array)
                )
            )
            bam_compression_cmd_process = subprocess.Popen(
                bam_compression_array,
                stdin=main_cmd_process.stdout,
                stdout=subprocess.PIPE,
            )

            # Attempt to capture stderr from main process if finished
            main_cmd_process.stdout.close()

        return bam_compression_cmd_process

    def _generate_samtools_main_command(self, file_uri, is_header, region, filter_dict):
        cmd_array = [self.settings["SAMTOOLS_EXTPATH"], "view"]

        if is_header:
            cmd_array.append("-Hb1")

        else:
            cmd_array.append("-h")

            if "mapq" in filter_dict:
                cmd_array.extend(["-q", filter_dict["mapq"]])

        mito_re = re.compile(r"^([Cc][Hh][Rr])?[mM][tT]?:")
        if "downsample" in filter_dict:
            cmd_array.extend(["-s", filter_dict["downsample"]])

        elif mito_re.match(region):
            cmd_array.extend(["-s", str(self.settings["downsample_rate"])])

        cmd_array.append(file_uri)

        if region:
            if region == "*":
                cmd_array.append("'*'")
            else:
                cmd_array.append(region)

        return cmd_array

    def get_bcftools_command(self, file_path, is_header, is_body, region, filter_dict):
        """"""

        base = [self.settings["BCFTOOLS_EXTPATH"], "view", "--no-version"]

        filter_content = []

        if "samples" in filter_dict:
            filter_content += ["-s", filter_dict["samples"]]

        # if the variants are sample-filtered, the header must be as well
        if is_header:
            return base + filter_content + ["-h", file_path]

        if "quality" in filter_dict:
            filter_content += ["-f", filter_dict["quality"]]

        if is_body:
            if region:
                return base + filter_content + ["-H", "-r", region, file_path]
            return base + filter_content + ["-H", file_path]

        if region:
            return base + filter_content + ["-r", region, file_path]
        return base + filter_content + [file_path]

    def _get_subprocess_out_and_stderr(self, cmd_line_args):
        """
        run the subprocess and get some output
        """

        subprocess_open = subprocess.Popen(
            cmd_line_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        content, stderr = subprocess_open.communicate(
            timeout=self.settings["SUBPROCESS_TIMEOUT"]
        )
        subprocess_open.stdout.close()
        subprocess_open.stderr.close()

        return content, stderr

    def validate_input(self, args):
        reads_mode = False
        header = False
        body = False

        if args["format"] in self.settings["READS_FORMAT"]:
            reads_mode = True

        if "class" in args:
            if args["class"].lower() == "header":
                header = True

            if args["class"].lower() == "body":
                body = True

        region = self.request.headers.get(self.settings["SUBREGION_HEADER_LABEL"], "")
        if body and reads_mode:
            self.validate_region(region)

        if not header and not body and reads_mode:
            raise HtsGetHttpError(
                reason="Request class  must be one of the following: body/header",
                status_code=400,
                error="InvalidRequest",
            )

        return header, body, region

    @retry(
        reraise=True,
        stop=stop_after_attempt(Settings["GLOBAL_RETRIES"]),
        wait=wait_random_exponential(
            multiplier=Settings["GLOBAL_BASE_BACKOFF"],
            max=Settings["GLOBAL_MAX_BACKOFF"],
        ),
        before_sleep=before_sleep_log(logging.getLogger(), logging.WARNING),
        retry=retry_if_not_exception_type(
            (HtsGetInvalidUrlWarning, HtsGetInvalidUrlError)
        ),
    )
    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        args = self._get_args("data")
        is_header, is_body, region = self.validate_input(args)

        # Login into OpenCGA
        session = yield IOLoop.current().run_in_executor(
            thread_pool_executor,
            partial(
                self.authentication_manager_class.login,
                token=self.request.headers.get("Authorization", ""),
                **self.settings,
            ),
        )

        if "study" not in args or "fileId" not in args:
            raise HtsGetHttpError(
                reason="study and fieldId arguments must be present",
                status_code=400,
                error="InvalidRequest",
            )

        cache_key = "study={}&fileId={}".format(args["study"], args["fileId"])
        query_file = self.cache["user_cache"].get(cache_key)

        if query_file is None:
            query_file = yield IOLoop.current().run_in_executor(
                thread_pool_executor,
                partial(
                    session.get_file,
                    file_id="byid/" + args["study"] + "/" + args["fileId"],
                    data_type="data",
                ),
            )
            self.cache["user_cache"][cache_key] = query_file

        file_path = query_file.file_path

        ref, start, end = self._get_coordinates(args, query_file)

        # fields = args.get("fields", None)
        # if fields:
        #     failing_flags = Utilities.validate_samflags(fields, self.settings['BAM_FIELDS'])
        #     if failing_flags:
        #         raise HtsGetHttpError(
        #             reason="Requested fields {} are not valid SAMFlags".format(failing_flags),
        #             status_code=400,
        #             error="InputError"
        #         )

        filters = args.get("filters", None)
        if filters:
            filter_dict = Utilities.parse_url_filters(filters, self.settings)
        else:
            filter_dict = dict()

        # this check excludes the filter dictionary, as the header requires a filter
        if is_header and any([ref, start, end]):
            raise HtsGetHttpError(
                reason="Positional parameters provided with class=header",
                status_code=400,
                error="InvalidInput",
            )

        if args["format"] in self.settings["READS_FORMAT"]:
            header_length = self.request.headers.get(
                self.settings["HEADER_LENGTH_LABEL"]
            )

            if not is_header:
                if header_length is None:
                    logging.error(
                        "Body request must specify {}".format(
                            self.settings["HEADER_LENGTH_LABEL"]
                        )
                    )
                    raise HtsGetHttpError(
                        log_message="Body request must specify {}.format(self.settings['HEADER_LENGTH_LABEL'])",
                        reason="Body request must specify {}".format(
                            self.settings["HEADER_LENGTH_LABEL"]
                        ),
                        status_code=400,
                        error="InvalidRequest",
                    )
                header_length = int(header_length)

            if args["format"] == "BAM":
                self.set_header("Content-Type", "application/vnd.ga4gh.bam")
            elif args["format"] == "CRAM":
                self.set_header("Content-Type", "application/vnd.ga4gh.cram")

            data = yield IOLoop.current().run_in_executor(
                thread_pool_executor,
                partial(
                    self._get_data_response,
                    format=args["format"],
                    file_uri=query_file.file_path,
                    region=region,
                    is_header=is_header,
                    filter_dict=filter_dict,
                    header_length=header_length,
                    eof=self.request.headers.get("Eof"),
                ),
            )

            self.write(data)
            self.flush()
            self.finish()

        elif args["format"] in self.settings["VARIANTS_FORMAT"]:
            self.set_header("Content-Type", "application/vnd.ga4gh.vcf")

            region = Utilities.cast_url_region_as_string(ref, start, end)

            header_key = "vcf_header::{}::{}".format(
                filter_dict.get("samples", ""), file_path
            )
            if is_header:
                header_content = self.cache["header_cache"].get(header_key)
                if header_content is not None:
                    byte_stream = io.BytesIO(header_content)

                    for data_chunk in self._yield_chunks(
                        byte_stream, self.settings["CHUNK_SIZE_IN_BYTES"]
                    ):
                        self.write(data_chunk)
                    self.finish()
                    return

            bcftools_cmds = self.get_bcftools_command(
                file_path, is_header, is_body, region, filter_dict
            )
            try:
                bcftools_content, stderr = yield IOLoop.current().run_in_executor(
                    thread_pool_executor,
                    partial(
                        self._get_subprocess_out_and_stderr, cmd_line_args=bcftools_cmds
                    ),
                )
            except subprocess.TimeoutExpired:
                logging.error(
                    'subprocess.TimeoutExpired running BCFTools: "{}". File visible: {}'.format(
                        " ".join(bcftools_cmds), os.path.exists(file_path)
                    ),
                    exc_info=True,
                )
                raise HtsGetHttpError(
                    reason='BCFTools command timed out: "{}"'.format(
                        " ".join(bcftools_cmds)
                    ),
                    status_code=400,
                    error="InvalidRequest",
                )

            # check for any thrown errors
            if stderr:
                self._identify_bcftools_error_category(stderr)

            if is_header:
                self.cache["header_cache"][header_key] = bcftools_content

            byte_stream = io.BytesIO(bcftools_content)

            for data_chunk in self._yield_chunks(
                byte_stream, self.settings["CHUNK_SIZE_IN_BYTES"]
            ):
                self.write(data_chunk)

        else:
            raise HtsGetHttpError(
                reason="Requested format is not supported",
                status_code=400,
                error="UnsupportedFormat",
            )

    def head(self, *args, **kwargs):
        self.finish()
