#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from collections import defaultdict
import json
import re

from htsget_errors.htsget_http_error import HtsGetHttpError


class Utilities:
    _ref_types = ["fa", "fasta"]

    @staticmethod
    def validate_samflags(input_flags, setting_flags):
        """

        :param input_flags:
        :param setting_flags:
        :return: any failing flags
        """

        fail_flags = []
        for flag in input_flags.split(","):
            clean_flag = flag.replace("SAM_", "")
            if clean_flag not in setting_flags:
                fail_flags.append(flag)

        return ",".join(fail_flags)

    @staticmethod
    def translate_field_to_filter_flag(field_list):
        """
        a list of field definitions, pre-filtered to retain only valid fields
        :param field_list:
        :return:
        """

        # clean fields
        clean_fields = [x.replace("SAM_", "") for x in field_list.split(",")]

        field_lookup = {
            "QNAME": 1,
            "FLAG": 2,
            "RNAME": 4,
            "POS": 8,
            "MAPQ": 16,
            "CIGAR": 32,
            "RNEXT": 64,
            "PNEXT": 128,
            "TLEN": 256,
            "SEQ": 512,
            "QUAL": 1024,
            "AUX": 2048,
            "RGAUX": 4096,
        }

        count = sum([field_lookup[x] for x in clean_fields])

        hex_count = hex(count)

        # return fully populated filter String
        return ["--input-fmt-option", "required_fields={}".format(hex_count)]

    @staticmethod
    def validate_coordinates(
        ref, start, end, end_of_span=None, chunk_size=None, data_endpoint=False
    ):
        """
        centralising all the validation of start and stop coordinates
        can be called from any other location to reduce code redundancy
        :param ref: reference chrom
        :param start: start position (0-based)
        :param end: end position (0-based, inclusive)
        :param end_of_span: if populated this can check if an end is larger than chrom length
        :return:
        """

        if start or end:
            if ref is None or ref == "*":
                raise HtsGetHttpError(
                    reason="Start &/or End defined without Reference",
                    status_code=400,
                    error="InvalidInput",
                )

            if start and start < 0:
                raise HtsGetHttpError(
                    reason="Start below 0",
                    status_code=400,
                    error="InvalidRange",
                )

            if data_endpoint and not all([start, end]):
                raise HtsGetHttpError(
                    reason="Data endpoint query with only start or end",
                    status_code=400,
                    error="InvalidRange",
                )

            # both populated and range invalid - only relevant if both populated
            if (start and end) and (start > end):
                raise HtsGetHttpError(
                    reason="Region values invalid ({}-{}), end coordinate should be bigger than start".format(
                        start, end
                    ),
                    status_code=400,
                    error="InvalidRange",
                )

            if end and end_of_span and end > end_of_span:
                raise HtsGetHttpError(
                    reason="End outside of contig span (end: {}, range: {})".format(
                        end, end_of_span
                    ),
                    status_code=400,
                    error="InvalidRange",
                )

            if start and end and chunk_size:
                if end - start > chunk_size:
                    raise HtsGetHttpError(
                        reason="{}-{} span is bigger than expected ({})".format(
                            start, end, chunk_size
                        ),
                        status_code=400,
                        error="InvalidRequest",
                    )

    @staticmethod
    def fill_out_coordinate_string(ref, start, end, file_object):
        """
        completes the parameters, so that there are either no values or all are present
        """

        # this method shouldn't have been called, plan for the worst
        if not any([ref, start, end]):
            return ""

        template_string = "&referenceName={chr}&start={start}&end={end}"

        if not start:
            start = 0

        if not end:
            end = file_object.contig_length[ref]

        return template_string.format(chr=ref, start=start, end=end)

    @staticmethod
    def cast_url_region_as_string(ref, start, end):
        """
        To be used by the data endpoint
        :param ref:
        :param start:
        :param end:
        :return:
        """

        if ref == "*":
            return ref

        if start == 0:
            start = str(start)

        if end == 0:
            end = str(end)

        elif all([ref, start, end]):
            return "{}:{}-{}".format(ref, start, end)

        elif ref:
            return ref

        else:
            return False

    @staticmethod
    def get_regions(start, end, size):
        next_start = start
        next_end = start + size - 1

        while next_end < end:
            yield next_start, next_end
            next_start = next_start + size
            next_end = next_end + size

        if next_end >= end:
            yield next_start, end

    @staticmethod
    def check_chromosome_prefix_in_reference(reference, contigs):
        """
        takes the contigs available for the file in question and verifies that the requested chromosome is valid
        includes pass-thru translation of M <-> MT
        :param reference:
        :param contigs:
        :return:
        """

        if not reference:
            return reference

        if reference == "*" or reference in contigs:
            return reference

        elif reference.lower() in contigs:
            return reference.lower()

        # error message should report back exactly what was queried
        og_reference = str(reference)

        # modify if appropriate, then retry same check
        if reference and reference not in contigs:

            # regex for mitochondrial chromosome - there can be only one
            mito_re = re.compile(r"^([Cc][Hh][Rr])?[mM][tT]?$")
            if mito_re.match(reference):
                possible_m = [m for m in contigs if mito_re.match(m)]
                if possible_m:
                    reference = possible_m[0]

            elif reference.lower().startswith("chr"):
                reference = reference.lower().replace("chr", "")

            else:
                reference = "chr{}".format(reference)

        if reference not in contigs:
            raise HtsGetHttpError(
                reason="Requested reference {} does not exist".format(og_reference),
                status_code=400,
                error="InvalidRange",
            )

        return reference

    @staticmethod
    def htsget_regions_dict(ref, start, end, contigs, contig_lengths, fragment_size):
        """
        allow for unspecified or "*" referenceName
        for contig == *, return * only
        for each undefined start/end pair, iterate across full chromosome in chunks
        for undefined chromosome, iterate across all contigs in genome in chunks
        for chrom start end all defined, call get_regions chunk generator
        """

        return_dictionary = defaultdict(list)

        if ref == "*":
            return ["*"]

        elif ref:
            # pad out any unpopulated variables
            if not start:
                start = 0

            if not end:
                end = contig_lengths[ref]

            for _start, _end in Utilities.get_regions(start, end, fragment_size):
                return_dictionary[ref].append([_start, _end])

            return dict(return_dictionary)

        # no reference provided - all contigs, all lengths
        for contig in contigs:
            start = 0
            end = contig_lengths[contig]

            for _start, _end in Utilities.get_regions(start, end, fragment_size):
                return_dictionary[contig].append([_start, _end])

        return dict(return_dictionary)

    @staticmethod
    def parse_url_filters(filter_string, app_settings, check_only=False):
        """
        :param filter_string: the json-list format filter string from the URL, format [FLAG=VALUE,THIS=THAT]
        """

        filter_dict = defaultdict(list)

        string_filters = app_settings["STRING_FILTERS"]
        float_filters = app_settings["FLOAT_FILTERS"]
        int_filters = app_settings["INT_FILTERS"]

        # break filter string into component parts
        filters_as_list = json.loads(filter_string)

        for filter in filters_as_list:
            filter_name, filter_value = filter.split("=")
            if filter_name in string_filters:

                def _method(x):
                    return str(x)

            elif filter_name in float_filters:

                def _method(x):
                    return float(x)

            elif filter_name in int_filters:

                def _method(x):
                    return int(x)

            else:
                raise HtsGetHttpError(
                    reason="Filter {} is not understood by the server".format(
                        filter_name
                    ),
                    status_code=400,
                    error="InvalidInput",
                )

            try:
                _method(filter_value)
                filter_dict[filter_name].append(filter_value)
            except ValueError:
                raise HtsGetHttpError(
                    reason="Filter {}={} was not provided in the correct format".format(
                        filter_name, filter_value
                    ),
                    status_code=400,
                    error="InvalidInput",
                )

        filter_dict = dict(filter_dict)

        if "mapq" in filter_dict:
            if len(filter_dict["mapq"]) > 1:
                raise HtsGetHttpError(
                    reason="multiple MapQ values provided: {}".format(
                        ",".join(filter_dict["mapq"])
                    ),
                    status_code=400,
                    error="InvalidInput",
                )
            filter_dict["mapq"] = filter_dict["mapq"][0]

        if "downsample" in filter_dict:
            if len(filter_dict["downsample"]) > 1:
                raise HtsGetHttpError(
                    reason="multiple downsample values provided: {}".format(
                        ",".join(filter_dict["downsample"])
                    ),
                    status_code=400,
                    error="InvalidInput",
                )
            filter_dict["downsample"] = filter_dict["downsample"][0]

        # if multiple samples are defined, each can be comma-sep string lists - merge all
        if "samples" in filter_dict:
            if len(filter_dict["samples"]) > 1:
                sample_set = set()
                for samples in filter_dict["samples"]:
                    sample_set.update(samples.split(","))

                filter_dict["samples"] = ",".join(sorted(sample_set))
            else:
                filter_dict["samples"] = filter_dict["samples"][0]

        # if multiple samples are defined, each can be comma-sep string lists - merge all
        if "quality" in filter_dict:
            if len(filter_dict["quality"]) > 1:
                sample_set = set()
                for quality in filter_dict["quality"]:
                    if "," in quality:
                        sample_set.update(quality.split(","))
                    else:
                        sample_set.add(quality)

                filter_dict["quality"] = ",".join(sorted(sample_set))
            else:
                filter_dict["quality"] = filter_dict["quality"][0]
        if check_only:
            return
        return dict(filter_dict)
