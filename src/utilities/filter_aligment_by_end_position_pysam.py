import sys

import pysam


def filter_reads_by_end(line, end_pos, outfile):
    if line.reference_end is None or line.reference_end <= end_pos:
        outfile.write(line)


if __name__ == "__main__":
    end_pos = int(sys.argv[1])
    infile = pysam.AlignmentFile("-", "r")
    outfile = pysam.AlignmentFile("-", "wh", template=infile)
    for line in infile:
        filter_reads_by_end(line, end_pos, outfile)
