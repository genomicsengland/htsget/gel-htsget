#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import subprocess

from htsget_errors.htsget_http_error import HtsGetHttpError


class SamtoolsRunner:
    def __init__(self, **kwargs):
        if kwargs["subprocess_timeout"] and kwargs["samtools_expath"]:
            self.subprocess_timeout = kwargs["subprocess_timeout"]
            self.samtools_expath = kwargs["samtools_expath"]
        else:
            raise HtsGetHttpError(reason="Invalid Input", status_code=400)

    def get_header_length(self, file_uri, file_format):

        array_command = [self.samtools_expath, "view", "-bH1"]

        if file_format == "BAM":
            eof_length = 28

        elif file_format == "CRAM":
            eof_length = 12
        else:
            raise HtsGetHttpError(reason="Invalid Input", status_code=400)

        array_command.append(file_uri)

        sub_process_header = subprocess.run(
            array_command,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            timeout=self.subprocess_timeout,
            check=True,
        )
        return len(sub_process_header.stdout) - eof_length
