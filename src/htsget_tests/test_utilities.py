import json
import pytest
import unittest

from mock import MagicMock, patch

from htsget_tests.htsget_test_case import HtsGetTestCase
from htsget_errors.htsget_http_error import HtsGetHttpError
from utilities.samtools_runner import SamtoolsRunner
from utilities.utilities import Utilities


class TestUtilities(HtsGetTestCase):
    @patch("tornado.web.RequestHandler.write")
    def test_validate_start_end_asterisk_raises_400(self, write):

        with self.assertRaises(HtsGetHttpError):
            Utilities.validate_coordinates("*", 1, 2)

        try:
            Utilities.validate_coordinates("*", 1, 2)
        except HtsGetHttpError as err:
            self.assertEqual(err.status_code, 400)
            self.assertEqual(err.reason, "Start &/or End defined without Reference")
            self.assertEqual(err.error, "InvalidInput")

    @patch("tornado.web.RequestHandler.write")
    def test_validate_start_end_wrong_order_raises_400(self, write):

        with self.assertRaises(HtsGetHttpError):
            Utilities.validate_coordinates("chr1", 2, 1)

        try:
            Utilities.validate_coordinates("chr1", 2, 1)
        except HtsGetHttpError as err:
            self.assertEqual(err.status_code, 400)
            self.assertEqual(
                err.reason,
                "Region values invalid (2-1), end coordinate should be bigger than start",
            )
            self.assertEqual(err.error, "InvalidRange")

    @patch("tornado.web.RequestHandler.write")
    def test_validate_start_below_1(self, write):

        with self.assertRaises(HtsGetHttpError):
            Utilities.validate_coordinates("chr1", start=-11, end=1)

        try:
            Utilities.validate_coordinates("chr1", start=-11, end=1)
        except HtsGetHttpError as err:
            self.assertEqual(err.status_code, 400)
            self.assertEqual(err.reason, "Start below 0")
            self.assertEqual(err.error, "InvalidRange")

    @patch("tornado.web.RequestHandler.write")
    def test_validate_end_outside_span(self, write):

        with self.assertRaises(HtsGetHttpError):
            Utilities.validate_coordinates("chr1", start=1, end=111, end_of_span=11)

        try:
            Utilities.validate_coordinates("chr1", start=1, end=111, end_of_span=11)
        except HtsGetHttpError as err:
            self.assertEqual(err.status_code, 400)
            self.assertEqual(
                err.reason, "End outside of contig span (end: 111, range: 11)"
            )
            self.assertEqual(err.error, "InvalidRange")

    @patch("tornado.web.RequestHandler.write")
    def test_validate_span_larger_than_chunk(self, write):

        with self.assertRaises(HtsGetHttpError):
            Utilities.validate_coordinates("chr1", start=1, end=111, chunk_size=11)

        try:
            Utilities.validate_coordinates("chr1", start=1, end=111, chunk_size=11)
        except HtsGetHttpError as err:
            self.assertEqual(err.status_code, 400)
            self.assertEqual(err.reason, "1-111 span is bigger than expected (11)")
            self.assertEqual(err.error, "InvalidRequest")

    def test_validate_samflags(self):

        good_flags = ["SPAM", "EGGS", "BISCUITS", "SAM_FLAG"]

        no_failing_flags = Utilities.validate_samflags("SPAM,EGGS", good_flags)
        self.assertEqual(no_failing_flags, "")

        no_failing_flags = Utilities.validate_samflags("SAM_SPAM,SAM_EGGS", good_flags)
        self.assertEqual(no_failing_flags, "")

        one_failing_flag = Utilities.validate_samflags("SPAM_SPAM,EGGS", good_flags)
        self.assertEqual(one_failing_flag, "SPAM_SPAM")

        two_failing_flags = Utilities.validate_samflags(
            "SPAM_SPAM,EGGS,MADETHISONEUP", good_flags
        )
        self.assertEqual(two_failing_flags, "SPAM_SPAM,MADETHISONEUP")

    def test_htsget_regions_dict(self):
        result_dict = Utilities.htsget_regions_dict(
            "chr1", 1, 34, ["chr1"], {"chr1": 123123}, 99999
        )
        self.assertEqual(["chr1"], list(result_dict.keys()))
        self.assertEqual({"chr1": [[1, 34]]}, result_dict)

    def test_htsget_regions_dict_chunked_region(self):
        result_dict = Utilities.htsget_regions_dict(
            "chr1", 1, 34, ["chr1"], {"chr1": 123123}, 33
        )
        self.assertEqual(["chr1"], list(result_dict.keys()))
        self.assertEqual({"chr1": [[1, 33], [34, 34]]}, result_dict)

    def test_htsget_regions_dict_chunked_to_end(self):
        result_dict = Utilities.htsget_regions_dict(
            "chr1", 1, None, ["chr1"], {"chr1": 123123}, 33
        )
        self.assertEqual(["chr1"], list(result_dict.keys()))
        self.assertIn([1, 33], result_dict["chr1"])

    def test_htsget_regions_dict_no_start(self):
        result_dict = Utilities.htsget_regions_dict(
            "chr1", None, 34, ["chr1"], {"chr1": 123123}, 33
        )
        self.assertEqual(["chr1"], list(result_dict.keys()))
        self.assertEqual({"chr1": [[0, 32], [33, 34]]}, result_dict)

    def test_htsget_regions_dict_no_regions(self):
        result_dict = Utilities.htsget_regions_dict(
            None, None, None, ["chr1", "chr2"], {"chr1": 123123, "chr2": 123123}, 999999
        )
        self.assertEqual(["chr1", "chr2"], list(result_dict.keys()))
        self.assertEqual({"chr1": [[0, 123123]], "chr2": [[0, 123123]]}, result_dict)

    def test_htsget_regions_dict_ref_star(self):
        result_dict = Utilities.htsget_regions_dict("*", None, None, [], {}, 0)
        self.assertEqual(["*"], result_dict)

    def test_parse_url_filters_pass_string(self):
        app_settings = {
            "STRING_FILTERS": ["A_STRING", "B_STRING", "samples", "quality"],
            "FLOAT_FILTERS": ["FLOATY", "downsample"],
            "INT_FILTERS": ["INTY", "mapq"],
        }

        filter_string = json.dumps(["A_STRING=THIS"])
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual({"A_STRING": ["THIS"]}, result_dict)

        filter_string = json.dumps(["A_STRING=THIS", "B_STRING=THAT"])
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual({"A_STRING": ["THIS"], "B_STRING": ["THAT"]}, result_dict)

        filter_string = json.dumps(["FLOATY=0.01"])
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual({"FLOATY": ["0.01"]}, result_dict)

        filter_string = json.dumps(["mapq=22"])
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual({"mapq": "22"}, result_dict)

        # I think this is the only error checking possible... all other data types are compatible transitions
        filter_string = json.dumps(["mapq=0.001"])
        with self.assertRaises(HtsGetHttpError):
            result_dict = Utilities.parse_url_filters(
                filter_string=filter_string, app_settings=app_settings
            )

        # mix it up
        filter_string = json.dumps(["mapq=66", "FLOATY=0.001", "A_STRING=FOO"])
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual(
            {"mapq": "66", "FLOATY": ["0.001"], "A_STRING": ["FOO"]}, result_dict
        )

        # multiple mapq
        filter_string = json.dumps(["mapq=66", "mapq=1", "A_STRING=FOO"])
        with self.assertRaises(HtsGetHttpError):
            result_dict = Utilities.parse_url_filters(
                filter_string=filter_string, app_settings=app_settings
            )

        # multiple downsample
        filter_string = json.dumps(["downsample=66", "downsample=1", "A_STRING=FOO"])
        with self.assertRaises(HtsGetHttpError):
            result_dict = Utilities.parse_url_filters(
                filter_string=filter_string, app_settings=app_settings
            )

        # multiple samples
        filter_string = json.dumps(
            ["samples=SPAM,EGGS", "samples=MORESPAM", "A_STRING=FOO"]
        )
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual(
            {"samples": "EGGS,MORESPAM,SPAM", "A_STRING": ["FOO"]}, result_dict
        )

        # multiple qualities
        filter_string = json.dumps(
            ["quality=SPAM,EGGS", "quality=MORESPAM", "A_STRING=FOO"]
        )
        result_dict = Utilities.parse_url_filters(
            filter_string=filter_string, app_settings=app_settings
        )
        self.assertEqual(
            {"quality": "EGGS,MORESPAM,SPAM", "A_STRING": ["FOO"]}, result_dict
        )


@pytest.mark.parametrize(
    "flags, result",
    [
        ("QNAME", ["--input-fmt-option", "required_fields=0x1"]),
        ("SAM_QNAME", ["--input-fmt-option", "required_fields=0x1"]),
        ("QNAME,POS", ["--input-fmt-option", "required_fields=0x9"]),
        ("QNAME,POS,RNAME", ["--input-fmt-option", "required_fields=0xd"]),
        ("RGAUX,POS", ["--input-fmt-option", "required_fields=0x1008"]),
    ],
)
def test_bitflags(flags, result):
    assert Utilities.translate_field_to_filter_flag(flags) == result


@pytest.mark.parametrize(
    "reference, contigs",
    [
        ("1", ["this_contig_will_not_be_matched"]),
        ("11", ["1"]),
    ],
)
def test_reference_contig_fails(reference, contigs):
    with pytest.raises(HtsGetHttpError):
        Utilities.check_chromosome_prefix_in_reference(reference, contigs)  # noqa: F841


@pytest.mark.parametrize(
    "chr, ref, alt, result",
    [
        ("*", None, None, "*"),
        ("*", 1, 2, "*"),
        ("chr1", None, 1, "chr1"),
        ("chr1", 0, 1, "chr1:0-1"),
        ("potato", "eggs", "bacon", "potato:eggs-bacon"),
        ("spam", None, None, "spam"),
        ("chr1", 1, 1, "chr1:1-1"),
        ("chr1", 1, "1", "chr1:1-1"),
        ("chr1", "1", 1, "chr1:1-1"),
        ("chr1", "1", "1", "chr1:1-1"),
        (1, "1", "1", "1:1-1"),
    ],
)
def test_param_cast_url_region_as_string(chr, ref, alt, result):
    assert Utilities.cast_url_region_as_string(chr, ref, alt) == result


@pytest.mark.parametrize(
    "reference, contigs, expected",
    [
        ("chr1", ["chr1", "chr2"], "chr1"),
        ("1", ["1"], "1"),
        ("chr1", ["1"], "1"),
        ("1", ["chr1"], "chr1"),
        ("Chr1", ["chr1"], "chr1"),
        ("ChR1", ["chr1"], "chr1"),
        ("CHR1", ["chr1"], "chr1"),
        ("CHRM", ["chrM"], "chrM"),
        ("chrM", ["chrM"], "chrM"),
        ("chrMT", ["chrMT"], "chrMT"),
        ("chrMT", ["chrM"], "chrM"),
        ("chrM", ["chrMT"], "chrMT"),
    ],
)
def test_param_reference_contig_successes(reference, contigs, expected):
    reference = Utilities.check_chromosome_prefix_in_reference(reference, contigs)
    assert reference == expected


class SamtoolsTestCase(HtsGetTestCase):
    """Unit tests specifically for the samtools runner script."""

    # Necessary for all three of the below samtools tests.
    sr = SamtoolsRunner(subprocess_timeout=5, samtools_expath="/bin/samtools")

    @patch("subprocess.run")
    def test_explicit_reference_pointed_to_if_bam(self, mock_run):
        """Check that BAM files are referenced with corresponding .bai files."""
        mock_run.return_value = MagicMock(stdout="stdout", stderr="stderr")
        self.sr.get_header_length("file://fake/path/to/test.bam", "BAM")  # noqa: F841
        mock_run.assert_called_with(
            [
                "/bin/samtools",
                "view",
                "-bH1",
                "file://fake/path/to/test.bam",
            ],
            stdout=-1,
            stderr=-2,
            timeout=5,
            check=True,
        )

    @patch("subprocess.run")
    def test_explicit_reference_pointed_to_if_cram(
        self,
        mock_run,
    ):
        """Check that CRAM files are referenced with corresponding .crai files."""
        mock_run.return_value = MagicMock(stdout="stdout", stderr="stderr")
        self.sr.get_header_length("file://fake/path/to/test.cram", "CRAM")  # noqa: F841
        mock_run.assert_called_with(
            [
                "/bin/samtools",
                "view",
                "-bH1",
                "file://fake/path/to/test.cram",
            ],
            stdout=-1,
            stderr=-2,
            timeout=5,
            check=True,
        )

    @patch("subprocess.run")
    def test_raises_exception_if_vcf(self, mock_run):
        """Check that VCF files are not given any explicit reference file."""
        mock_run.return_value = MagicMock(stdout="stdout", stderr="stderr")
        with self.assertRaises(HtsGetHttpError):
            self.sr.get_header_length(
                "file://fake/path/to/test.vcf", "VCF"
            )  # noqa: F841


if __name__ == "__main__":
    unittest.main()
