#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import json
import pytest
from urllib.parse import quote

from mock import patch, MagicMock

from htsget_tests.htsget_test_case import HtsGetTestCase, override_settings
from htsget_errors.htsget_http_error import HtsGetHttpError
from utilities.utilities import Utilities


VCF_PAYLOAD_FILE_INFO = {
    "id": "999999",
    "uri": "not_a_real_file_path",
    "study": "study",
    "attributes": {
        "variantFileMetadata": {
            "header": {
                "complexLines": [
                    {
                        "id": "chr19",
                        "key": "contig",
                        "genericFields": {"length": 40000000},
                    }
                ]
            }
        }
    },
}


def validate_response(test_instance, under_test, **kwargs):
    """
    arbitrary response validation using a set list of keys
    :param under_test: a dictionary of content
    :param kwargs: key-value arguments for all indices to check
    :return: True for checked and accurate, else False
    """

    test_instance.assertIn("htsget", under_test)

    # pull out main body of response
    data_under_test = under_test["htsget"]
    test_instance.assertIn("format", data_under_test)

    # explicit checking - if format in kwargs, expected in response
    if "format" in kwargs:
        test_instance.assertEqual(data_under_test["format"], kwargs["format"])

    test_instance.assertEqual(type(data_under_test["urls"]), list)

    # explicit checking of expected URLs
    if "urls" in kwargs:
        # retain final sections
        response_urls = {
            url_section["url"].split("/")[-1] for url_section in data_under_test["urls"]
        }

        expected_url_set = set(kwargs["urls"])

        # allow for incomplete matching
        if "open" in kwargs and kwargs["open"]:
            test_instance.assertTrue(expected_url_set - response_urls, set())
        else:
            test_instance.assertEqual(response_urls, expected_url_set)

    # final url section marked as Eof
    test_instance.assertEqual(data_under_test["urls"][-1]["headers"]["Eof"], "true")

    return True


@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
class TestVariantsByID(HtsGetTestCase):
    def test_vcf_by_id(self, get_file_by_sample_name, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        expected = {
            "format": "VCF",
            "urls": [
                "data?format=VCF&study=123&fileId=999999&class=header",
                "data?format=VCF&study=123&fileId=999999&class=body&referenceName=chr19&start=39885781&end=39885781",
            ],
        }
        self.assertEqual(response.code, 200)
        # then content assertions - shared method
        self.assertTrue(
            validate_response(
                self, json.loads(response.body.decode("utf-8")), **expected
            )
        )

    def test_vcf_by_id_with_filter(
        self, get_file_by_sample_name, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781&filters={}".format(
                quote(json.dumps(["samples=SAM1"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )

        expected = {
            "format": "VCF",
            "urls": [
                "data?format=VCF&study=123&fileId=999999&class=header&filters={}".format(
                    quote(json.dumps(["samples=SAM1"]))
                ),
                "data?format=VCF&study=123&fileId=999999&class=body&referenceName=chr19&start=39885781&end=39885781&filters={}".format(  # noqa: E501
                    quote(json.dumps(["samples=SAM1"]))
                ),
            ],
        }
        self.assertEqual(response.code, 200)
        # then content assertions - shared method
        self.assertTrue(
            validate_response(
                self, json.loads(response.body.decode("utf-8")), **expected
            )
        )

    def test_vcf_by_id_header_only(
        self, get_file_by_file_id, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456?class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )

        expected = {
            "format": "VCF",
            "urls": ["data?format=VCF&study=123&fileId=999999&class=header"],
        }
        self.assertEqual(response.code, 200)
        # then content assertions
        self.assertTrue(
            validate_response(
                self, json.loads(response.body.decode("utf-8")), **expected
            )
        )

    def test_vcf_byid_no_coordinates(
        self, get_file_by_file_id, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456", headers={"Authorization": "Bearer TOKEN"}
        )
        expected = {
            "format": "VCF",
            "urls": [
                "data?format=VCF&study=123&fileId=999999&class=header",
                "data?format=VCF&study=123&fileId=999999&class=body",
            ],
        }
        self.assertEqual(response.code, 200)
        # then content assertions
        self.assertTrue(
            validate_response(
                self, json.loads(response.body.decode("utf-8")), **expected
            )
        )

    def test_vcf_byid_invalid_position_ordering(
        self, get_file_by_file_id, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456?referenceName=chr19&start=49785781&end=29935781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)

    def test_vcf_byid_invalid_reference(
        self, get_file_by_file_id, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456?referenceName=chrP&start=49785781&end=29935781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.reason, "Start greater than end")
        self.assertEqual(response.code, 400)

    def test_vcf_byid_invalid_class(
        self, get_file_by_file_id, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456?class=fake",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.reason, "Requested class is not supported")
        self.assertEqual(response.code, 400)

    def test_vcf_byid_invalid_path(
        self, get_file_by_sample_name, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/byid/123/456.tbi?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.reason, "File extension is not supported")
        self.assertEqual(response.code, 400)

        response = self.fetch(
            "/variants/byid/123/456.csi?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.reason, "File extension is not supported")
        self.assertEqual(response.code, 400)


@override_settings(
    opencga_vcf_types=["study SMALL ~.duprem.left.split.reheadered.vcf.gz$"]
)
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_sample_name",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
class TestVariantsByName(HtsGetTestCase):
    def test_vcf_name(self, get_file_by_sample_name, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/study/SMALL/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)


@override_settings(
    opencga_vcf_types=["study SMALL ~.duprem.left.split.reheadered.vcf.gz$"]
)
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_path",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
class TestVariantsByPath(HtsGetTestCase):
    def test_vcf_bypath(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)

        expected = {
            "format": "VCF",
            "urls": [
                "data?format=VCF&study=1053593329&fileId=999999&class=header",
                "data?format=VCF&study=1053593329&fileId=999999&class=body&referenceName=chr19&start=39885781&end=39885781",  # noqa: E501
            ],
        }

        # then content assertions - shared method
        self.assertTrue(
            validate_response(
                self, json.loads(response.body.decode("utf-8")), **expected
            )
        )

    def test_vcf_no_coordinates(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)
        expected = {
            "format": "VCF",
            "urls": [
                "data?format=VCF&study=1053593329&fileId=999999&class=header",
                "data?format=VCF&study=1053593329&fileId=999999&class=body",
            ],
        }

        # then content assertions - shared method
        self.assertTrue(
            validate_response(
                self, json.loads(response.body.decode("utf-8")), **expected
            )
        )

    def test_vcf_bypath_wrong_contig(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "referenceName=chr199&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message, "Requested reference chr199 does not exist"
        )

        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "referenceName=*&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message, "referenceName * is invalid for variant requests"
        )

    def test_vcf_bypath_no_ref(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            "Coordinates are specified and either no reference is specified or "
            "referenceName is *",
        )

    def test_vcf_bypath_header_no_ref(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)

    def test_vcf_bypath_header_no_ref_one_sample(
        self, get_file, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "class=header&filters={}".format(quote(json.dumps(["samples=SAM1"]))),
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)

    def test_vcf_bypath_header_with_ref(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "class=header&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message, "Class argument not valid with other parameters"
        )

    def test_vcf_bypath_ref_just_number(self, get_file, OpenCGAClient, isfile, exists):
        response = self.fetch(
            "/variants/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.vcf.gz?"
            "referenceName=19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 200)


class TestVariantsNoMock(HtsGetTestCase):
    def test_variant_head(self):
        response = self.fetch("/variants/id", method="HEAD")
        self.assertEqual(response.code, 200)

    def test_variant_options(self):
        response = self.fetch(
            "/variants/id",
            headers={
                "Access-Control-Request-Method": "GET",
                "Access-Control-Request-Headers": "True",
            },
            method="OPTIONS",
        )
        self.assertEqual(response.code, 204)


@pytest.mark.parametrize(
    "reference,contigs,expected",
    [
        pytest.param("chr1", ["chr1", "chr2"], "chr1"),
        pytest.param("1", ["1"], "1"),
        pytest.param("chr1", ["1"], "1"),
        pytest.param("1", ["chr1"], "chr1"),
    ],
)
def test_that_chromosome_is_selected_with_different_naming_conventions(
    reference, contigs, expected
):
    reference = Utilities.check_chromosome_prefix_in_reference(reference, contigs)
    assert reference == expected


def test_that_exception_is_raised_if_reference_not_in_contig():
    with pytest.raises(HtsGetHttpError):
        Utilities.check_chromosome_prefix_in_reference(
            reference="1", contigs=["this_contig_will_not_be_matched"]
        )
