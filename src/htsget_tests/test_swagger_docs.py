#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import os

from htsget_tests.htsget_test_case import HtsGetTestCase, override_settings


@override_settings(
    SWAGGER_PATH=os.path.join(
        os.path.dirname(os.path.dirname(__file__)), "swagger.yaml"
    )
)
class TestVariantsByName(HtsGetTestCase):
    """"""

    def test_swagger(self):
        response = self.fetch("/api/docs")
        self.assertEqual(response.code, 200)

    def test_swagger_yaml(self):
        response = self.fetch("/swagger.yaml")
        self.assertEqual(response.code, 200)
