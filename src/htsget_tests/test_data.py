#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import json
import os
import subprocess
from urllib.parse import quote

from mock import patch, MagicMock

from handlers.data_handler import DataHandler
from htsget_tests.htsget_test_case import HtsGetTestCase, override_settings


VCF_PAYLOAD_FILE_INFO = {
    "id": "11111",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [
                {"sequenceName": "chr9", "sequenceLength": 49885781},
                {"sequenceName": "chr19", "sequenceLength": 49885781},
                {"sequenceName": "chrMT", "sequenceLength": 49885781},
            ]
        }
    },
}

CRAM_PAYLOAD_FILE_INFO = {
    "id": "999999",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [
                {"sequenceName": "chr19", "sequenceLength": 49885781}
            ]
        }
    },
}


@override_settings(
    REFRESH_CACHE=True,  # this has to be overridden to stop data cross-pollinating from other cache use
)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch.object(DataHandler, "_get_subprocess_out_and_stderr")
@patch("tornado.web.RequestHandler.write")
class TestDataHandlerTimeoutVCF(HtsGetTestCase):
    def test_vcf_subprocess_timeout(
        self, write, mock_subprocess, exists, isfile, get_file_by_id, OpenCGAClient
    ):
        mock_subprocess.side_effect = subprocess.TimeoutExpired(cmd="cmd", timeout=20)
        response = self.fetch(
            "/data?format=VCF&study=99999&fileId=8888888&class=body&referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            'BCFTools command timed out: "bcftools view --no-version -H -r '
            'chr19:39885781-39885781 not_a_real_file_path"',
        )


@override_settings(
    OPENCGA_CONF="opencga.json",
    VCF_TYPES="vcf_types.tsv",
    GENOMES_MOUNT_PATHS=[os.path.dirname(__file__)],
)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=CRAM_PAYLOAD_FILE_INFO,
)
@patch("subprocess.Popen", return_value=MagicMock())
@patch("tornado.web.RequestHandler.write")
class TestDataHandlerSubprocess(HtsGetTestCase):
    def test_bam_data_header(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        subprocess.assert_called_with(
            ["samtools", "view", "-Hb1", "not_a_real_file_path"], stderr=-1, stdout=-1
        )
        subprocess.return_value.communicate.assert_called_with(timeout=40)
        self.assertEqual(response.code, 200)

    def test_bam_data_header_sample_filtered(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=header&filters={}".format(
                quote(json.dumps(["samples=SAM1"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        subprocess.assert_called_with(
            ["samtools", "view", "-Hb1", "not_a_real_file_path"], stderr=-1, stdout=-1
        )
        subprocess.return_value.communicate.assert_called_with(timeout=40)
        self.assertEqual(response.code, 200)

    def test_cram_data(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=CRAM&study=99999&fileId=8888888&class=body",
            headers={
                "X-Subregion": "chr19:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "200",
            },
        )

        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")

        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "not_a_real_file_path",
                "chr19:39885781-39885781",
            ],
        )
        self.assertEqual(subprocess.call_count, 3)
        self.assertEqual(response.code, 200)

    def test_mitochondrial_call(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        response = self.fetch(  # noqa: F841
            "/data?format=CRAM&study=99999&fileId=8888888&class=body",
            headers={
                "X-Subregion": "MT:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "200",
            },
        )
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "-s",
                str(self.settings["downsample_rate"]),
                "not_a_real_file_path",
                "MT:39885781-39885781",
            ],
        )

    def test_bam_unmapped(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body&referenceName=*",
            headers={
                "X-Subregion": "*",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            ["samtools", "view", "-h", "not_a_real_file_path", "'*'"],
        )
        self.assertEqual(subprocess.call_count, 1)
        self.assertEqual(response.code, 200)

    def test_cram_unmapped(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=CRAM&study=99999&fileId=8888888&class=body&referenceName=*",
            headers={
                "X-Subregion": "*",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "not_a_real_file_path",
                "'*'",
            ],
        )
        self.assertEqual(subprocess.call_count, 1)
        self.assertEqual(response.code, 200)

    # only works with CRAM?
    # def test_bam_data_with_fields(self, write, subprocess,
    #                               get_file_by_id, exists, isfile, OpenCGAClient):
    #
    #     subprocess.return_value.communicate.return_value = [b"result", b'']
    #     response = self.fetch(
    #         '/data?format=BAM&study=99999&fileId=8888888&class=body&fields=RNAME,POS',
    #         headers={"X-Subregion": "chr19:39885781-39885781", "Authorization": "Bearer TOKEN",
    #                  "Header-bytes-length": "2000"}
    #     )
    #     get_file_by_id.assert_called_with(file_id='8888888', opencga_study='99999')
    #     self.assertEqual(subprocess.mock_calls[0].args[0],
    #                       ['samtools', 'view', '-h', 'not_a_real_file_path', 'chr19:39885781-39885781'])
    #     self.assertEqual(subprocess.mock_calls[2].args[0], ['samtools', 'view', '-b', '-1'])
    #     self.assertEqual(subprocess.call_count, 3)
    #     self.assertEqual(response.code, 200)

    def test_bam_data_with_one_filter(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body&filters={}".format(
                quote(json.dumps(["mapq=55"]))
            ),
            headers={
                "X-Subregion": "chr19:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "-q",
                "55",
                "not_a_real_file_path",
                "chr19:39885781-39885781",
            ],
        )
        self.assertEqual(
            subprocess.mock_calls[2].args[0], ["samtools", "view", "-b", "-1"]
        )
        self.assertEqual(subprocess.call_count, 3)
        self.assertEqual(response.code, 200)

    def test_bam_data_with_two_filters(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body&filters={}".format(
                quote(json.dumps(["mapq=55", "quality=PASS"]))
            ),
            headers={
                "X-Subregion": "chr19:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "-q",
                "55",
                "not_a_real_file_path",
                "chr19:39885781-39885781",
            ],
        )
        self.assertEqual(
            subprocess.mock_calls[2].args[0], ["samtools", "view", "-b", "-1"]
        )
        self.assertEqual(subprocess.call_count, 3)
        self.assertEqual(response.code, 200)

    def test_bam_data_with_multi_sample_filters(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body&filters={}".format(
                quote(json.dumps(["samples=A,B", "samples=C"]))
            ),
            headers={
                "X-Subregion": "chr19:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "not_a_real_file_path",
                "chr19:39885781-39885781",
            ],
        )
        self.assertEqual(
            subprocess.mock_calls[2].args[0], ["samtools", "view", "-b", "-1"]
        )
        self.assertEqual(subprocess.call_count, 3)
        self.assertEqual(response.code, 200)

    def test_bam_data(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body",
            headers={
                "X-Subregion": "chr19:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[0].args[0],
            [
                "samtools",
                "view",
                "-h",
                "not_a_real_file_path",
                "chr19:39885781-39885781",
            ],
        )
        self.assertEqual(subprocess.call_count, 3)
        self.assertEqual(response.code, 200)

        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body",
            headers={
                "X-Subregion": "chr19:39885781-39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "2000",
                "Eof": "true",
            },
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(
            subprocess.mock_calls[3].args[0],
            [
                "samtools",
                "view",
                "-h",
                "not_a_real_file_path",
                "chr19:39885781-39885781",
            ],
        )
        self.assertEqual(subprocess.call_count, 5)
        self.assertEqual(response.code, 200)

    def test_param_errors(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            "Request class  must be one of the following: body/header",
        )

        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&end=39885781&class=body",
            headers={
                "X-Subregion": "chr19:39885782-59885781",
                "Authorization": "Bearer TOKEN",
            },
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            "39885782-59885781 span is bigger than expected ({})".format(
                self.settings.get("SUBREGION_CHUNK_SIZE")
            ),
        )

        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888" "&end=39885781&class=body",
            headers={
                "X-Subregion": "chr19:39885781-45",
                "Authorization": "Bearer TOKEN",
            },
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            "Region values invalid (39885781-45), end coordinate should be bigger than start",
        )

        response = self.fetch(
            "/data?format=NOTVALID&study=99999&fileId=8888888"
            "&end=39885781&class=body",
            headers={
                "X-Subregion": "chr19:39885781",
                "Authorization": "Bearer TOKEN",
                "Header-bytes-length": "200",
            },
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(response.error.message, "Requested format is not supported")

        response = self.fetch(
            "/data?format=BAM&study=99999&fileId=8888888&class=body",
            headers={"X-Subregion": "chr19:39885781", "Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            "Please provide a valid X-Subregion value: `contig:start-end`",
        )


@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("subprocess.Popen", retun_value=MagicMock())
class TestVCFCalls(HtsGetTestCase):
    def test_vcf_data_valid_region_real_mock_call(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&referenceName=chr19&start=0&end=99999&class=body",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            [
                "bcftools",
                "view",
                "--no-version",
                "-H",
                "-r",
                "chr19:0-99999",
                "not_a_real_file_path",
            ],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_valid_region_call(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&referenceName=chr19&start=39885781&end=39885781&class=body",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            [
                "bcftools",
                "view",
                "--no-version",
                "-H",
                "-r",
                "chr19:39885781-39885781",
                "not_a_real_file_path",
            ],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_whole_vcf_call(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            [
                "bcftools",
                "view",
                "--no-version",
                "-r",
                "chr19:39885781-39885781",
                "not_a_real_file_path",
            ],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_unmapped(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&referenceName=*",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message, "referenceName * is invalid for variant requests"
        )

    def test_vcf_data_header_call(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            ["bcftools", "view", "--no-version", "-h", "not_a_real_file_path"],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_header_call_sample_filtered(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header&filters={}".format(
                quote(json.dumps(["samples=SAM1"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            [
                "bcftools",
                "view",
                "--no-version",
                "-s",
                "SAM1",
                "-h",
                "not_a_real_file_path",
            ],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_call_ineffective_filter(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=body&filters={}".format(
                quote(json.dumps(["mapq=55"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            ["bcftools", "view", "--no-version", "-H", "not_a_real_file_path"],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_call_sample_filter(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=body&filters={}".format(
                quote(json.dumps(["samples=LP"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            [
                "bcftools",
                "view",
                "--no-version",
                "-s",
                "LP",
                "-H",
                "not_a_real_file_path",
            ],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_functional_test(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=1054027298&referenceName=chr9",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            ["bcftools", "view", "--no-version", "-r", "chr9", "not_a_real_file_path"],
        )
        get_file_info.assert_called_with(
            file_id="1054027298", opencga_study="1053593329"
        )
        self.assertEqual(response.code, 200)

    def test_vcf_data_call_both_filters(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=body&filters={}".format(
                quote(json.dumps(["samples=LP", "quality=FAIL"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            [
                "bcftools",
                "view",
                "--no-version",
                "-s",
                "LP",
                "-f",
                "FAIL",
                "-H",
                "not_a_real_file_path",
            ],
            mock_subprocess.call_args_list[0].args[0],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_call_2_sample_filters(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=body&filters={}".format(
                quote(json.dumps(["samples=LP1,LP2", "samples=LP3"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            [
                "bcftools",
                "view",
                "--no-version",
                "-s",
                "LP1,LP2,LP3",
                "-H",
                "not_a_real_file_path",
            ],
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 200)

    def test_vcf_data_error_call(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [
            b"result",
            b"Could not retrieve index file for",
        ]

        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            ["bcftools", "view", "--no-version", "-h", "not_a_real_file_path"],
        )
        self.assertEqual(
            response.error.message,
            "BCFTools failed - no index present for requested file",
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 400)

    def test_vcf_data_unknown_error_call(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [
            b"result",
            b"SCREAM INTO THE VOID",
        ]
        mock_subprocess.resultcode = 1  # subprocess failed
        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(
            mock_subprocess.call_args_list[0].args[0],
            ["bcftools", "view", "--no-version", "-h", "not_a_real_file_path"],
        )
        self.assertEqual(
            response.error.message, "Unknown BCFTools error: SCREAM INTO THE VOID"
        )
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")
        self.assertEqual(response.code, 400)
