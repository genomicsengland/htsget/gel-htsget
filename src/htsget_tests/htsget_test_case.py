#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from tornado.testing import AsyncHTTPTestCase

from htsget_server.htsget_app import Htsget
from htsget_server.settings import Settings


def get_settings(**kwargs):
    settings = Settings
    for option in kwargs:
        settings[option] = kwargs[option]

    return settings


class override_settings(object):
    def __init__(self, **options):
        self.options = options

    def __call__(self, cls):
        NewHtsGetTestCase = cls

        def setUp(inner_self):
            inner_self.settings = get_settings(**self.options)
            super(cls, inner_self).setUp()  # pylint: disable=[E1003]

        NewHtsGetTestCase.setUp = setUp
        return NewHtsGetTestCase


class HtsGetTestCase(AsyncHTTPTestCase):
    def setUp(self):
        self.settings = get_settings()
        super(HtsGetTestCase, self).setUp()

    def get_app(self):
        return Htsget(application_settings=self.settings).application
