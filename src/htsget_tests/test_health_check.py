#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import json
import os
from datetime import datetime

import httpretty
import pytest
from mock import patch

from health.enums import Status
from health.health_checks import HealthChecks, health_checks
from health.resources.opencga import OpenCGA as OpencgaResource
from htsget_server.settings import Settings
from htsget_tests.htsget_test_case import HtsGetTestCase, override_settings

HEALTH_CHECK_TOKEN = "token"


class TestHealthCheck(HtsGetTestCase):
    """"""

    def test_run(self):
        checks = HealthChecks()
        self.assertIsNone(checks.resources[0].last_checked)
        checks.run(skip_resources=[])
        self.assertIsNotNone(checks.resources[0].last_checked)

    def test_run_cached(self):
        checks = HealthChecks()
        self.assertIsNone(checks.resources[0].last_checked)
        checks.run(skip_resources=[])
        self.assertIsNotNone(checks.resources[0].last_checked)
        last_check = checks.resources[0].last_checked
        checks.run(skip_resources=[])
        self.assertEqual(last_check, checks.resources[0].last_checked)

    def test_dict(self):
        data = health_checks.to_dict("/health/1/")
        self.assertIn("status", data)
        self.assertIn("datetime", data)
        self.assertIn("serviceName", data)

    def test_dict_show_dependencies(self):
        data = health_checks.to_dict("/health/1/", show_dependencies=True)
        self.assertIn("dependencies", data)

    def test_status_compare(self):
        ok = Status.OK
        ok2 = Status.OK
        down = Status.DOWN
        not_conf = Status.NOT_CONFIGURED

        assert ok >= ok2
        assert ok > down
        assert not_conf < ok
        assert not_conf <= ok


@override_settings(
    GENOMES_MOUNT_PATHS=[os.path.dirname(__file__)],
    HEALTH_CHECK_TOKEN=HEALTH_CHECK_TOKEN,
    HEALTH_CHECK_CACHE_SECONDS=0.00000,
)
class TestHealthCheckResponse(HtsGetTestCase):
    def test_health_check_with_token(self):
        url = "/health/1/?token=" + Settings["HEALTH_CHECK_TOKEN"]
        response = self.fetch(url, method="GET")
        self.assertIn(response.code, [200, 503])
        json_response_dependencies = json.loads(response.body)["dependencies"]
        self.assertIn("apis", json_response_dependencies)
        self.assertIn("datastores", json_response_dependencies)

    def test_health_check_with_skip(self):
        opencga = OpencgaResource()
        url = (
            "/health/1/?token="
            + Settings["HEALTH_CHECK_TOKEN"]
            + "&skip="
            + opencga.name
        )
        response = self.fetch(url, method="GET")
        self.assertIn(response.code, [200, 503])

        json_response_dependencies = json.loads(response.body)["dependencies"]
        self.assertIn("apis", json_response_dependencies)
        self.assertIn("datastores", json_response_dependencies)

        service_descriptions = [
            dependency["description"]
            for dependency in json_response_dependencies["datastores"]
        ]
        self.assertNotIn(opencga.name, service_descriptions)

    def test_health_check_no_token(self):
        response = self.fetch("/health/1/", method="GET")
        self.assertIn(response.code, [200, 503])
        self.assertNotIn("dependencies", json.loads(response.body))

    def test_health_check_head(self):
        response = self.fetch("/health/1/", method="HEAD")
        self.assertIn(response.code, [200, 503])

    @patch("os.listdir", return_value=[])
    def test_health_check_empty_dir(self, listdir):
        response = self.fetch("/health/1/", method="GET")
        self.assertEqual(response.code, 503)


@override_settings(
    GENOMES_MOUNT_PATHS=["/invalid/test/path/"], HEALTH_CHECK_CACHE_SECONDS=0.00000
)
class TestHealthCheckPathNotFound(HtsGetTestCase):
    def test_health_check(self):

        url = "/health/1/?token=" + Settings["HEALTH_CHECK_TOKEN"]
        response = self.fetch(url, method="GET")

        self.assertEqual(response.code, 503)

        message = Settings["GENOMES_MOUNT_PATHS"][0] + " is not mounted"
        json_response_mount_description = json.loads(response.body)["dependencies"][
            "datastores"
        ][0]["description"]

        self.assertEqual(json_response_mount_description, message)


@override_settings(
    opencga_host="http://fake-opencga.valid", HEALTH_CHECK_CACHE_SECONDS=0.00000
)
class TestOpenCGAConnects(HtsGetTestCase):
    def test_that_opencga_connects(self):

        opencga = OpencgaResource()
        opencga_url = Settings["opencga_host"] + opencga.health_path
        opencga_payload = json.dumps({"response": [{"result": [{"ok": True}]}]})

        httpretty.enable()
        httpretty.register_uri(
            httpretty.GET, opencga_url, status=200, body=opencga_payload
        )

        opencga.check()
        self.assertEqual(Status.OK, opencga.status)
        httpretty.disable()


@override_settings(
    opencga_host="http://fake-opencga.invalid", HEALTH_CHECK_CACHE_SECONDS=0.00000
)
class TestOpenCGANotConnects(HtsGetTestCase):
    def test_that_opencga_not_connects(self):

        opencga = OpencgaResource()
        opencga_url = Settings["opencga_host"] + opencga.health_path
        opencga_payload = json.dumps({"status": "DEGRADED"})
        httpretty.enable()
        httpretty.register_uri(
            httpretty.GET, opencga_url, status=503, body=opencga_payload
        )

        opencga.check()
        self.assertEqual(Status.DEGRADED, opencga.status)
        httpretty.disable()


class TestPingHandler(HtsGetTestCase):
    """"""

    def test_ping(self):
        url = "/ping/"
        response = self.fetch(url, method="GET")
        self.assertEqual(response.code, 200)


@pytest.mark.parametrize(
    ["uri", "expected"],
    [
        [
            {
                "method": httpretty.GET,
                "uri": "http://opencga/opencga/webservices/rest/v1/meta/health",
                "status": 200,
                "body": json.dumps({"status": "OK"}),
            },
            {
                "status": "OK",
                "datetime": "2023-01-01T00:00:00Z",
                "requestUrl": "/health/1/",
                "serviceName": "htsget",
                "components": ["OpenCGA"],
            },
        ],
        [
            {
                "method": httpretty.GET,
                "uri": "http://opencga/opencga/webservices/rest/v1/meta/health",
                "status": 200,
                "body": json.dumps({"status": "DOWN"}),
            },
            {
                "status": "DOWN",
                "datetime": "2023-01-01T00:00:00Z",
                "requestUrl": "/health/1/",
                "serviceName": "htsget",
                "components": ["OpenCGA"],
                "unavailableComponents": ["OpenCGA"],
            },
        ],
        [
            {
                "method": httpretty.GET,
                "uri": "http://opencga/opencga/webservices/rest/v1/meta/health",
                "status": 200,
                "body": json.dumps({"status": "DEGRADED"}),
            },
            {
                "status": "DEGRADED",
                "datetime": "2023-01-01T00:00:00Z",
                "requestUrl": "/health/1/",
                "serviceName": "htsget",
                "components": ["OpenCGA"],
                "unavailableComponents": ["OpenCGA"],
            },
        ],
        [
            {
                "method": httpretty.GET,
                "uri": "http://opencga/opencga/webservices/rest/v1/meta/health",
                "status": 503,
                "body": json.dumps({"status": "DEGRADED"}),
            },
            {
                "status": "DEGRADED",
                "datetime": "2023-01-01T00:00:00Z",
                "requestUrl": "/health/1/",
                "serviceName": "htsget",
                "components": ["OpenCGA"],
                "unavailableComponents": ["OpenCGA"],
            },
        ],
        [
            {
                "method": httpretty.GET,
                "uri": "http://opencga/opencga/webservices/rest/v1/meta/health",
                "status": 503,
                "body": json.dumps({"status": "OK"}),
            },
            {
                "status": "OK",
                "datetime": "2023-01-01T00:00:00Z",
                "requestUrl": "/health/1/",
                "serviceName": "htsget",
                "components": ["OpenCGA"],
            },
        ],
        [
            {
                "method": httpretty.GET,
                "uri": "http://opencga/opencga/webservices/rest/v1/meta/health",
                "status": 500,
                "body": json.dumps({"status": "OK"}),
            },
            {
                "status": "DOWN",
                "datetime": "2023-01-01T00:00:00Z",
                "requestUrl": "/health/1/",
                "serviceName": "htsget",
                "components": ["OpenCGA"],
                "unavailableComponents": ["OpenCGA"],
            },
        ],
    ],
)
def test_opencga(uri: dict, expected: dict):
    checks = HealthChecks(
        now=lambda: datetime(year=2023, month=1, day=1),
        resources=[
            OpencgaResource(
                url="http://opencga/opencga",
                health_path="/webservices/rest/v1/meta/health",
            )
        ],
    )

    httpretty.enable()
    httpretty.register_uri(**uri)

    actual = checks.to_dict("/health/1/")

    httpretty.disable()

    assert actual == expected
