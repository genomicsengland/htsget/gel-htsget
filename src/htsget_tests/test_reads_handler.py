#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import json
from urllib.parse import quote

from mock import patch, MagicMock

from htsget_tests.htsget_test_case import HtsGetTestCase


CRAM_PAYLOAD_FILE_INFO = {
    "id": "999999",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [
                {"sequenceName": "chr19", "sequenceLength": 40000001}
            ]
        }
    },
}

CRAM_PAYLOAD_FILE_INFO_CONTIG_NUMBER = {
    "id": "999999",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [{"sequenceName": "19", "sequenceLength": 40000001}]
        }
    },
}


CRAM_PAYLOAD_FILE_INFO_MITOCHONDRIAL = {
    "id": "999999",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [{"sequenceName": "chrM", "sequenceLength": 40000001}]
        }
    },
}


def validate_response(
    test_case,
    response,
    expected_format,
    expected_class,
    expected_file_id,
    expected_subregions,
    **kwargs
):
    response = json.loads(response.body.decode("utf-8"))
    test_case.assertIn("htsget", response)
    test_case.assertIn("format", response["htsget"])
    test_case.assertEqual(response["htsget"]["format"], expected_format)
    for url in response["htsget"]["urls"]:
        test_case.assertIn(url["class"], expected_class)
        test_case.assertIn(expected_file_id, url["url"])
        if expected_subregions and url["class"] == "body":
            test_case.assertIn(url["headers"]["X-Subregion"], expected_subregions)

        # explicit checking of expected URLs
        if "urls" in kwargs:
            # retain final sections
            response_urls = {
                url_section["url"].split("/")[-1]
                for url_section in response["htsget"]["urls"]
            }

            expected_url_set = set(kwargs["urls"])

            # allow for incomplete matching, e.g. if result has tons of URLs this can check a subset
            if "open" in kwargs and kwargs["open"]:
                test_case.assertTrue(expected_url_set - response_urls, set())
            else:
                test_case.assertEqual(response_urls, expected_url_set)


@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch("subprocess.run", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_sample_name",
    return_value=CRAM_PAYLOAD_FILE_INFO,
)
class TestReadsByName(HtsGetTestCase):
    def test_byname_cram(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/1053593329/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        validate_response(
            self,
            response,
            expected_format="CRAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
        )
        self.assertEqual(response.code, 200)

    def test_byname_bam(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/1053593329/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781&format=BAM",
            headers={"Authorization": "Bearer TOKEN"},
        )
        validate_response(
            self,
            response,
            expected_format="BAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
        )
        self.assertEqual(response.code, 200)

    def test_byname_bam_with_filters(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/1053593329/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781&format=BAM&filters={}".format(  # noqa: E501
                quote(json.dumps(["samples=SAM1"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )

        expected_results = {
            "urls": [
                "data?format=BAM&study=1053593329&fileId=999999&class=body&referenceName=chr19&start=39885781&end=39885781&filters={}".format(  # noqa: E501
                    quote(json.dumps(["samples=SAM1"]))
                ),
                "data?format=BAM&study=1053593329&fileId=999999&class=header&filters={}".format(
                    quote(json.dumps(["samples=SAM1"]))
                ),
            ]
        }
        validate_response(
            self,
            response,
            expected_format="BAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
            **expected_results
        )
        self.assertEqual(response.code, 200)


@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch("subprocess.run", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_path",
    return_value=CRAM_PAYLOAD_FILE_INFO,
)
class TestReadsByPath(HtsGetTestCase):
    def test_bypath_cram(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.cram?"
            "referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        validate_response(
            self,
            response,
            expected_format="CRAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
        )
        self.assertEqual(response.code, 200)

    def test_bypath_bam(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        validate_response(
            self,
            response,
            expected_format="BAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
        )

    # def test_bypath_bam_with_fields(self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile):
    #     response = self.fetch(
    #         '/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?'
    #         'format=BAM&referenceName=chr19&start=39885781&end=39885781&fields=RNAME,POS',
    #         headers={"Authorization": "Bearer TOKEN"}
    #     )
    #     validate_response(self, response, expected_format='BAM', expected_class=['header', 'body'],
    #                       expected_file_id='999999', expected_subregions=['chr19:39885781-39885781'],
    #                       )
    #
    #     self.assertEqual(response.code, 200)

    def test_bypath_bam_with_filters(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781&filters={}".format(
                quote(json.dumps(["quality=PASS", "mapq=50"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        validate_response(
            self,
            response,
            expected_format="BAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
        )

        self.assertEqual(response.code, 200)

    def test_bypath_bam_with_bad_filters(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781&filters={}".format(
                quote(json.dumps(["mapq=0.50"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(400, response.code)
        self.assertEqual(
            "Filter mapq=0.50 was not provided in the correct format",
            response.error.message,
        )

        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781&filters={}".format(
                quote(json.dumps(["mapq=foo"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(400, response.code)
        self.assertEqual(
            "Filter mapq=foo was not provided in the correct format",
            response.error.message,
        )

        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781&filters={}".format(
                quote(json.dumps(["mapq=1", "mapq=2"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(400, response.code)
        self.assertEqual("multiple MapQ values provided: 1,2", response.error.message)

    # works only with cram output?
    # def test_bypath_bam_with_bad_fields(self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile):
    #     response = self.fetch(
    #         '/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?'
    #         'format=BAM&referenceName=chr19&start=39885781&end=39885781&fields=BADRNAME,POS',
    #         headers={"Authorization": "Bearer TOKEN"}
    #     )
    #
    #     self.assertEqual(response.code, 400)
    #     self.assertEqual(response.error.message, "Requested fields BADRNAME are not valid SAMFlags")

    def test_bypath_whole_contig_bam(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)
        response = json.loads(response.body.decode("utf-8"))
        # 402 = 401 chunks of 100000 pb urls + 1 header url
        self.assertEqual(len(response["htsget"]["urls"]), 402)

    def test_reads_no_coords(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?format=BAM",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)

    def test_reads_wrong_format(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=NOVALID&referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(response.error.message, "Requested format is not supported")

    def test_reads_not_int(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781.bai",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(
            response.error.message,
            "File extension is not supported",
        )

    def test_bam_replaced_by_cram(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.cram?"
            "format=BAM&referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        validate_response(
            self,
            response,
            expected_format="CRAM",
            expected_class=["header", "body"],
            expected_file_id="999999",
            expected_subregions=["chr19:39885781-39885781"],
        )

    def test_reads_start_end_error(
        self, get_file_by_path, subprocess, OpenCGAClient, exists, isfile
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.bam?"
            "format=BAM&referenceName=chr19&start=39885782&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 400)
        self.assertEqual(response.error.message, "Start greater than end")

    def test_reads_bypath_ref_just_number(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.cram?"
            "referenceName=19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 200)


class TestMisc(HtsGetTestCase):
    @patch("os.path.exists", return_value=True)
    @patch("os.path.isfile", return_value=True)
    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
    @patch("subprocess.run", return_value=MagicMock())
    @patch(
        "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_path",
        return_value=CRAM_PAYLOAD_FILE_INFO_CONTIG_NUMBER,
    )
    def test_reads_bypath_contig_just_number(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.cram?"
            "referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 200)

    @patch("os.path.exists", return_value=True)
    @patch("os.path.isfile", return_value=True)
    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
    @patch("subprocess.run", return_value=MagicMock())
    @patch(
        "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_path",
        return_value=CRAM_PAYLOAD_FILE_INFO_MITOCHONDRIAL,
    )
    def test_reads_bypath_mitochondrial(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/bypath/1053593329/by_name:LP3000243-DNA_C04:BE00000460:LP3000243-DNA_C04.cram?"
            "referenceName=M&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )

        self.assertEqual(response.code, 200)
