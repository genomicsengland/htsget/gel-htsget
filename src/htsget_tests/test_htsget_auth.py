#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from unittest import TestCase

from mock import patch, Mock, MagicMock

from htsget_auth.authentication_manager import AuthenticationManager, File
from htsget_auth.opencga_authentication_manager import (
    _load_vcf_types,
    OpencgaAuthenticationManager,
)
from htsget_errors.htsget_http_error import HtsGetHttpError


opencga_conf = {
    "opencga_host": "opencga_host",
    "opencga_version": "v1",
    "opencga_file_search_options": '{"attributes.gelStatus": "READY"}',
}
VCF_TYPES_MOCK = {"study": {"SMALL": "~.SMALL.vcf.gz$", "SNP": "~.SNP.vcf.gz$"}}


class OpenCGAResponseMockAux:
    def get(self):
        return ["val1", "val2"]


class OpenCGAResponseMockAux2:
    def get(self):
        return []


class AuthManagerTest(TestCase):
    def test_initiate_manager(self):
        with self.assertRaises(NotImplementedError):
            AuthenticationManager(token="Bearer Token", **{"settings": "fo"})

    def test_parse_token(self):
        self.assertEqual("Token", AuthenticationManager._get_session_id("Bearer Token"))
        with self.assertRaises(ValueError):
            self.assertEqual("Token", AuthenticationManager._get_session_id("Token"))

    def test_no_implemented_methods(self):
        with self.assertRaises(NotImplementedError):
            AuthenticationManager.login(token="Bearer Token", **{"settings": "fo"})
        with self.assertRaises(NotImplementedError):
            AuthenticationManager._configure({"settings": "fo"})

    def test_files_not_exists(self):
        with self.assertRaises(HtsGetHttpError) as cm:
            File(
                file_path="this/is/not/a/real/file.txt",
                contigs=[],
                file_id="f1",
                response_id="f1",
            )

        self.assertEqual(cm.exception.status_code, 404)
        self.assertIn(
            cm.exception.reason,
            "File does not exist on system: this/is/not/a/real/file.txt",
        )


class OpencgaAuthManagerTest(TestCase):
    def test_vcf_file_types_parsing(self):
        result = _load_vcf_types(
            [
                "1053593329 SNP .SNP.vcf.gz$",
                "1053593329 SMALL .SMALL.vcf.gz$",
                "TYPE SNP PATTERN PATTERN",
            ]
        )

        self.assertDictEqual(
            {"1053593329": {"SMALL": ".SMALL.vcf.gz$", "SNP": ".SNP.vcf.gz$"}}, result
        )

    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=Mock())
    def test_initialise_opencga_manager(self, opencga_client_mock):
        manager = OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        opencga_client_mock.assert_called_with(
            auto_refresh=False,
            configuration={
                "version": "v1",
                "rest": {"hosts": ["opencga_host"]},
                "retry": {
                    "max_attempts": 5,
                    "min_retry_seconds": 2,
                    "max_retry_seconds": 3,
                },
            },
            session_id="TOKEN",
        )

        self.assertEqual(manager.token, "Bearer TOKEN")

    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=Mock())
    def test_logout_opencga_manager(self, opencga_client_mock):
        manager = OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        manager.logout()
        self.assertIs(manager.session, None)

    @patch(
        "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager._convert_opencga_file",
        return_value=MagicMock(),
    )
    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
    @patch("os.path.isfile", return_value=True)
    def test_get_reads_files(
        self, isfile, opencga_client_mock, opencga_client_mock_convert_opencga_file
    ):
        manager = OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        manager.get_file(
            "bypath/study/path.cram", data_type="reads", reads_format="CRAM"
        )
        opencga_client_mock.return_value.files.search.assert_called_with(
            bioformat="ALIGNMENT",
            include="id,uri,attributes,study",
            path="path.cram",
            study="study",
            **{"attributes.gelStatus": "READY"}
        )
        manager.get_file("byid/study/file_id", data_type="reads", reads_format="CRAM")
        opencga_client_mock.return_value.files.search.assert_called_with(
            include="id,uri,attributes,study", id="file_id", study="study"
        )
        manager.get_file("study/sample", data_type="reads", reads_format="BAM")
        opencga_client_mock.return_value.files.search.assert_called_with(
            bioformat="ALIGNMENT",
            include="id,uri,attributes,study",
            samples="sample",
            name="~.bam$",
            study="study",
            **{"attributes.gelStatus": "READY"}
        )

    @patch(
        "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager._convert_opencga_file",
        return_value=MagicMock(),
    )
    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
    @patch("os.path.isfile", return_value=True)
    def test_get_variants_files(
        self, isfile, opencga_client_mock, opencga_client_mock_convert_opencga_file
    ):
        manager = OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        manager._vcf_types_regex = VCF_TYPES_MOCK
        manager.get_file("bypath/study/path.vcf.gz", data_type="variants")
        opencga_client_mock.return_value.files.search.assert_called_with(
            bioformat="VARIANT",
            include="id,uri,attributes,study",
            path="path.vcf.gz",
            study="study",
            **{"attributes.gelStatus": "READY"}
        )
        manager.get_file("byid/study/file_id", data_type="variants")
        opencga_client_mock.return_value.files.search.assert_called_with(
            include="id,uri,attributes,study", id="file_id", study="study"
        )

        manager.get_file("study/SNP/sample", data_type="variants")
        opencga_client_mock.return_value.files.search.assert_called_with(
            bioformat="VARIANT",
            include="id,uri,attributes,study",
            samples="sample",
            name="~.SNP.vcf.gz$",
            study="study",
            **{"attributes.gelStatus": "READY"}
        )
        with self.assertRaises(HtsGetHttpError) as cm:
            manager.get_file("study/SNPs/sample", data_type="variants")

        self.assertEqual(cm.exception.status_code, 400)
        self.assertEqual(cm.exception.reason, "Pattern for file type SNPs not found")

    @patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
    @patch("os.path.isfile", return_value=True)
    def test_get_files_exceptions(self, isfile, opencga_client_mock):
        manager = OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = None
            manager.get_file(
                "bypath/study/path.cram", data_type="reads", reads_format="CRAM"
            )
        self.assertIn(
            "Upstream server did not respond in a correct manner Response from upstream",
            cmd.exception.reason,
        )

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = OpenCGAResponseMockAux()
            manager.get_file(
                "bypath/study/path.cram", data_type="reads", reads_format="CRAM"
            )
        self.assertIn("Multiple files found for", cmd.exception.reason)

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = OpenCGAResponseMockAux2()
            manager.get_file(
                "bypath/study/path.cram", data_type="reads", reads_format="CRAM"
            )
        self.assertIn("No file found", cmd.exception.reason)

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = None
            manager.get_file("study/name", data_type="reads", reads_format="CRAM")
        self.assertIn(
            "Upstream server did not respond in a correct manner Response from upstream",
            cmd.exception.reason,
        )

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = OpenCGAResponseMockAux()
            manager.get_file("study/name", data_type="reads", reads_format="CRAM")
        self.assertIn("Multiple files found for", cmd.exception.reason)

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = OpenCGAResponseMockAux2()
            manager.get_file("study/name", data_type="reads", reads_format="CRAM")
        self.assertIn("No file found", cmd.exception.reason)

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = None
            manager.get_file("byid/study/name", data_type="reads", reads_format="CRAM")
        self.assertIn(
            "Upstream server did not respond in a correct manner Response from upstream",
            cmd.exception.reason,
        )

        with self.assertRaises(HtsGetHttpError) as cmd:
            manager.session.files.search.return_value = OpenCGAResponseMockAux2()
            manager.get_file("byid/study/name", data_type="reads", reads_format="CRAM")
        self.assertIn("No file", cmd.exception.reason)


class TestAuthErrors(TestCase):
    @patch(
        "pyCGA.opencgarestclients.OpenCGAClient",
        side_effect=Mock(side_effect=Exception('{"error": "INVALID TOKEN"}')),
    )
    def test_initialise_opencga_manager_invalid_login(self, opencga_client_mock):
        with self.assertRaises(HtsGetHttpError) as cm:
            OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        self.assertEqual(cm.exception.reason, "INVALID TOKEN")
        self.assertEqual(cm.exception.status_code, 401)

    @patch(
        "pyCGA.opencgarestclients.OpenCGAClient",
        side_effect=Mock(side_effect=Exception("NOT A JSON")),
    )
    def test_initialise_opencga_manager_unexpected_exception(self, opencga_client_mock):
        with self.assertRaises(HtsGetHttpError) as cm:
            OpencgaAuthenticationManager(token="Bearer TOKEN", **opencga_conf)
        self.assertEqual(cm.exception.reason, "NOT A JSON")
        self.assertEqual(cm.exception.status_code, 401)
