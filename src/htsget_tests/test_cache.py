#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import json
from urllib.parse import quote

from mock import patch, MagicMock

from htsget_tests.htsget_test_case import HtsGetTestCase, override_settings


CRAM_PAYLOAD_FILE_INFO = {
    "id": "999999",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [
                {"sequenceName": "chr19", "sequenceLength": 40000001}
            ]
        }
    },
}

VCF_PAYLOAD_FILE_INFO = {
    "id": "11111",
    "uri": "not_a_real_file_path",
    "study": "1053593329",
    "attributes": {
        "alignmentHeader": {
            "sequenceDiccionary": [
                {"sequenceName": "chr9", "sequenceLength": 49885781},
                {"sequenceName": "chr19", "sequenceLength": 49885781},
                {"sequenceName": "chrMT", "sequenceLength": 49885781},
            ]
        }
    },
}


@override_settings(
    REFRESH_CACHE=True
)  # this has to be overridden to stop data cross-pollinating from other cache use
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("subprocess.Popen", retun_value=MagicMock())
class TestVCFCacheCalls(HtsGetTestCase):
    def test_vcf_data_header_cache(
        self, mock_subprocess, isfile, exists, get_file_info, OpenCGAClient
    ):
        mock_subprocess.return_value.communicate.return_value = [b"result", b""]
        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(
            "/data?format=VCF&study=1053593329&fileId=11111&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(response.code, 200)
        self.assertEqual(mock_subprocess.call_count, 1)
        self.assertEqual(get_file_info.call_count, 1)
        get_file_info.assert_called_with(file_id="11111", opencga_study="1053593329")


@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=VCF_PAYLOAD_FILE_INFO,
)
class TestVariantsByID(HtsGetTestCase):
    def test_vcf_by_id(self, get_file_by_sample_name, OpenCGAClient, isfile, exists):
        response = self.fetch(  # noqa: F841
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(  # noqa: F841
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(  # noqa: F841
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(get_file_by_sample_name.call_count, 1)

    def test_vcf_by_id_with_filter(
        self, get_file_by_sample_name, OpenCGAClient, isfile, exists
    ):
        response = self.fetch(  # noqa: F841
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781&filters={}".format(
                quote(json.dumps(["samples=SAM1"]))
            ),
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(  # noqa: F841
            "/variants/byid/123/456?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(get_file_by_sample_name.call_count, 1)


@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch("subprocess.run", return_value=MagicMock())
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_sample_name",
    return_value=CRAM_PAYLOAD_FILE_INFO,
)
class TestReadsByName(HtsGetTestCase):
    def test_byname_cram(
        self,
        get_file_by_path,
        subprocess,
        OpenCGAClient,
        exists,
        isfile,
    ):
        response = self.fetch(
            "/reads/1053593329/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(
            "/reads/1053593329/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(
            "/reads/1053593329/LP3000243-DNA_C04?referenceName=chr19&start=39885781&end=39885781",
            headers={"Authorization": "Bearer TOKEN"},
        )
        self.assertEqual(get_file_by_path.call_count, 1)
        self.assertEqual(subprocess.call_count, 1)
        self.assertEqual(response.code, 200)


@patch("pyCGA.opencgarestclients.OpenCGAClient", return_value=MagicMock())
@patch("os.path.exists", return_value=True)
@patch("os.path.isfile", return_value=True)
@patch(
    "htsget_auth.opencga_authentication_manager.OpencgaAuthenticationManager.get_file_by_file_id",
    return_value=CRAM_PAYLOAD_FILE_INFO,
)
@patch("subprocess.Popen", return_value=MagicMock())
@patch("tornado.web.RequestHandler.write")
class TestDataHandlerCached(HtsGetTestCase):
    def test_bam_header_cache(
        self,
        write,
        subprocess,
        get_file_by_id,
        exists,
        isfile,
        OpenCGAClient,
    ):
        subprocess.return_value.communicate.return_value = [b"result", b""]

        response = self.fetch(  # noqa: F841
            "/data?format=BAM&study=99999&fileId=8888888&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        response = self.fetch(  # noqa: F841
            "/data?format=BAM&study=99999&fileId=8888888&class=header",
            headers={"Authorization": "Bearer TOKEN"},
        )
        get_file_by_id.assert_called_with(file_id="8888888", opencga_study="99999")
        self.assertEqual(get_file_by_id.call_count, 1)
        self.assertEqual(subprocess.call_count, 1)
        subprocess.assert_called_with(
            ["samtools", "view", "-Hb1", "not_a_real_file_path"], stderr=-1, stdout=-1
        )
        subprocess.return_value.communicate.assert_called_with(timeout=40)
