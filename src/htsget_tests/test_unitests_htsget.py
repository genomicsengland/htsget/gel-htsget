#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import unittest

from handlers.base_handler import BaseHandler
from htsget_tests.htsget_test_case import HtsGetTestCase

# to be deleted?


@unittest.skip  # type: ignore[arg-type]
class TestDataResponse(HtsGetTestCase):
    def calculate_byte_ranges(self, total_size, header_size, chunk_size):
        pass

    def test_create_response(self):
        start = 5000
        end = 1000000000

        expected_chunk_byte_range_content = [
            "bytes=5000-10005000",
            "bytes=10005000-20005000",
            "bytes=20005000-30005000",
            "bytes=30005000-40005000",
            "bytes=40005000-50005000",
            "bytes=50005000-60005000",
            "bytes=60005000-70005000",
            "bytes=70005000-80005000",
            "bytes=80005000-90005000",
            "bytes=90005000-100005000",
            "bytes=100005000-110005000",
            "bytes=110005000-120005000",
            "bytes=120005000-130005000",
            "bytes=130005000-140005000",
            "bytes=140005000-150005000",
            "bytes=150005000-160005000",
            "bytes=160005000-170005000",
            "bytes=170005000-180005000",
            "bytes=180005000-190005000",
            "bytes=190005000-200005000",
            "bytes=200005000-210005000",
            "bytes=210005000-220005000",
            "bytes=220005000-230005000",
            "bytes=230005000-240005000",
            "bytes=240005000-250005000",
            "bytes=250005000-260005000",
            "bytes=260005000-270005000",
            "bytes=270005000-280005000",
            "bytes=280005000-290005000",
            "bytes=290005000-300005000",
            "bytes=300005000-310005000",
            "bytes=310005000-320005000",
            "bytes=320005000-330005000",
            "bytes=330005000-340005000",
            "bytes=340005000-350005000",
            "bytes=350005000-360005000",
            "bytes=360005000-370005000",
            "bytes=370005000-380005000",
            "bytes=380005000-390005000",
            "bytes=390005000-400005000",
            "bytes=400005000-410005000",
            "bytes=410005000-420005000",
            "bytes=420005000-430005000",
            "bytes=430005000-440005000",
            "bytes=440005000-450005000",
            "bytes=450005000-460005000",
            "bytes=460005000-470005000",
            "bytes=470005000-480005000",
            "bytes=480005000-490005000",
            "bytes=490005000-500005000",
            "bytes=500005000-510005000",
            "bytes=510005000-520005000",
            "bytes=520005000-530005000",
            "bytes=530005000-540005000",
            "bytes=540005000-550005000",
            "bytes=550005000-560005000",
            "bytes=560005000-570005000",
            "bytes=570005000-580005000",
            "bytes=580005000-590005000",
            "bytes=590005000-600005000",
            "bytes=600005000-610005000",
            "bytes=610005000-620005000",
            "bytes=620005000-630005000",
            "bytes=630005000-640005000",
            "bytes=640005000-650005000",
            "bytes=650005000-660005000",
            "bytes=660005000-670005000",
            "bytes=670005000-680005000",
            "bytes=680005000-690005000",
            "bytes=690005000-700005000",
            "bytes=700005000-710005000",
            "bytes=710005000-720005000",
            "bytes=720005000-730005000",
            "bytes=730005000-740005000",
            "bytes=740005000-750005000",
            "bytes=750005000-760005000",
            "bytes=760005000-770005000",
            "bytes=770005000-780005000",
            "bytes=780005000-790005000",
            "bytes=790005000-800005000",
            "bytes=800005000-810005000",
            "bytes=810005000-820005000",
            "bytes=820005000-830005000",
            "bytes=830005000-840005000",
            "bytes=840005000-850005000",
            "bytes=850005000-860005000",
            "bytes=860005000-870005000",
            "bytes=870005000-880005000",
            "bytes=880005000-890005000",
            "bytes=890005000-900005000",
            "bytes=900005000-910005000",
            "bytes=910005000-920005000",
            "bytes=920005000-930005000",
            "bytes=930005000-940005000",
            "bytes=940005000-950005000",
            "bytes=950005000-960005000",
            "bytes=960005000-970005000",
            "bytes=970005000-980005000",
            "bytes=980005000-990005000",
            "bytes=990005000-1000000000",
        ]

        expected_no_of_chunks_length = len(expected_chunk_byte_range_content)

        data = BaseHandler._create_response(  # pylint: disable=[E1120, E1123]
            ref="chr1",
            start=12,
            end=12,
            host="host.com",
            reads_format=["BAM"],
            variants_format=["VCF"],
            format_="BAM",
            file_id="byid/12/12",
            token="token",
            start_point=start,
            total_size_of_data=end,
            chunk_size_in_bytes=10000000,
        )
        received_no_of_chunks_length = len(data["htsget"]["urls"]) - 1
        urls = data["htsget"]["urls"][1:]
        received_chunk_byte_range_content = []
        for url in urls:
            received_chunk_byte_range_content.append(url["headers"]["Range"])

        assert expected_no_of_chunks_length == received_no_of_chunks_length, (
            "create response method is not returning the requested number of byte chunks Expected "
            + str(expected_no_of_chunks_length)
            + " Recieved "
            + str(received_no_of_chunks_length)
        )

        assert (
            expected_chunk_byte_range_content == received_chunk_byte_range_content
        ), "Content of byte ranges does not match"
