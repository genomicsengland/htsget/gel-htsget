#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import json
import logging
import re
from typing import Dict

from pyCGA import opencgarestclients

from htsget_auth.authentication_manager import AuthenticationManager, File
from htsget_errors.htsget_http_error import HtsGetHttpError


def _load_vcf_types(opencga_vcf_types):
    vcf_types = {}
    for opencga_vcf_type in opencga_vcf_types:
        try:
            study, vcf_type, pattern = opencga_vcf_type.split(" ")
            vcf_types.setdefault(study, {}).setdefault(vcf_type, pattern)
        except ValueError:
            logging.info(
                "Variable {} was omitted because it not follow the expected pattern".format(
                    opencga_vcf_type
                )
            )
    return vcf_types


class OpencgaAuthenticationManager(AuthenticationManager):
    _include_file_fields = "id,uri,attributes,study"
    _regex_by_sample = re.compile(
        r"(?P<opencga_study>(?!bypath)(.*))/(?P<sample_name>(.*))"
    )
    _regex_by_vcf_type = re.compile(
        r"(?P<opencga_study>(?!bypath)(.*))/(?P<vcf_type>(.*))/(?P<sample_name>(.*))"
    )
    _regex_by_path = re.compile(
        r"bypath/(?P<opencga_study>(?!bypath)(.*))/(?P<path>(.*))"
    )
    _regex_by_id = re.compile(
        r"byid/(?P<opencga_study>(?!bypath)(.*))/(?P<file_id>(.*))"
    )
    _reads_format_name_regex = {"CRAM": "~.cram$", "BAM": "~.bam$"}
    _vcf_types_regex: Dict = {}

    @classmethod
    def login(cls, token, **kwargs):
        return_class = super(OpencgaAuthenticationManager, cls).login(token, **kwargs)
        return_class._vcf_types_regex = _load_vcf_types(
            return_class.options["opencga_vcf_types"]
        )
        return return_class

    @staticmethod
    def _login(token, options):
        opencga_config = OpencgaAuthenticationManager._configure(options)
        try:
            return opencgarestclients.OpenCGAClient(
                configuration=opencga_config, session_id=token, auto_refresh=False
            )

        except Exception as exception:
            try:
                msg = json.loads(str(exception))["error"]
            except (ValueError, KeyError):
                msg = str(exception)
            raise HtsGetHttpError(
                reason=msg, error="InvalidAuthentication", status_code=401
            )

    @staticmethod
    def _configure(options):
        return {
            "version": options.get("opencga_version"),
            "rest": {"hosts": [options.get("opencga_host")]},
            "retry": {
                "max_attempts": 5,
                "min_retry_seconds": 2,
                "max_retry_seconds": 3,
            },
        }

    def get_file(self, file_id, data_type, reads_format=None):
        try:
            parsed_id = self._parse_file_id(
                file_id=file_id, data_type=data_type, reads_format=reads_format
            )
            if "path" in parsed_id:
                return self._convert_opencga_file(
                    self.get_file_by_path(
                        path=parsed_id.get("path").replace(":", "/"),
                        file_format=parsed_id.get("file_format"),
                        opencga_study=parsed_id.get("opencga_study"),
                    ),
                    study=parsed_id.get("opencga_study"),
                )
            if "sample_name" in parsed_id:
                return self._convert_opencga_file(
                    self.get_file_by_sample_name(
                        sample_name=parsed_id.get("sample_name"),
                        file_format=parsed_id.get("file_format"),
                        file_name_regex=parsed_id.get("regex"),
                        opencga_study=parsed_id.get("opencga_study"),
                    ),
                    study=parsed_id.get("opencga_study"),
                )
            if "file_id" in parsed_id:
                return self._convert_opencga_file(
                    self.get_file_by_file_id(
                        file_id=parsed_id.get("file_id"),
                        opencga_study=parsed_id.get("opencga_study"),
                    ),
                    study=parsed_id.get("opencga_study"),
                )
        except HtsGetHttpError as exception:
            raise exception

        except Exception as exception:
            logging.error(
                msg="There was an error in the interaction with OpenCGA: The response from OpenCGA was "
                + str(exception)
            )
            raise HtsGetHttpError(
                reason="Upstream server did not respond in a correct manner Response from upstream: "
                + str(exception),
                error="Bad Gateway",
                status_code=504,
            )

    def get_file_by_path(self, path, file_format, opencga_study):
        search_options = json.loads(
            self.options.get("opencga_file_search_options", "{}")
        )
        file_info = self.session.files.search(
            study=opencga_study,
            bioformat=file_format,
            path=path,
            include=self._include_file_fields,
            **search_options,
        ).get()
        msg = (
            f'No file found "{path}" in study "{opencga_study}", '
            f"bioformat: {file_format}, search options: {search_options}"
        )
        logging.debug(msg)
        if not file_info:
            raise HtsGetHttpError(
                reason=msg,
                error="NotFound",
                status_code=404,
            )
        elif len(file_info) > 1:
            msg = 'Multiple files found for  "{}" in study "{}"'
            raise HtsGetHttpError(
                reason=msg.format(path, opencga_study),
                error="NotFound",
                status_code=404,
            )
        else:
            return file_info[0]

    def get_file_by_sample_name(
        self, sample_name, file_format, file_name_regex, opencga_study
    ):
        search_options = json.loads(
            self.options.get("opencga_file_search_options", "{}")
        )
        file_info = self.session.files.search(
            study=opencga_study,
            samples=sample_name,
            bioformat=file_format,
            name=file_name_regex,
            include=self._include_file_fields,
            **search_options,
        ).get()
        msg = (
            f'No file found for sample "{sample_name}" in study "{opencga_study}", '
            f"bioformat: {file_format}, name: {file_name_regex}, "
            f"include: {self._include_file_fields}, options: {search_options}"
        )
        logging.debug(msg)
        if not file_info:
            raise HtsGetHttpError(
                reason=msg,
                error="NotFound",
                status_code=404,
            )
        elif len(file_info) > 1:
            msg = 'Multiple files found for sample "{}" in study "{}"'
            raise HtsGetHttpError(
                reason=msg.format(sample_name, opencga_study),
                error="NotFound",
                status_code=404,
            )
        else:
            return file_info[0]

    def get_file_by_file_id(self, file_id, opencga_study):
        file_info = self.session.files.search(
            study=opencga_study, id=file_id, include=self._include_file_fields
        ).get()

        msg = f'No file "{file_id}" in study "{opencga_study}", search options: {self._include_file_fields}'
        logging.debug(msg)
        if not file_info:
            raise HtsGetHttpError(
                reason=msg,
                error="NotFound",
                status_code=404,
            )
        else:
            return file_info[0]

    def _parse_file_id(self, file_id, data_type, reads_format=None):
        if data_type == "reads":
            for regex in [
                self._regex_by_id,
                self._regex_by_path,
                self._regex_by_sample,
            ]:
                match = regex.match(file_id)
                if match:
                    parsed_id = match.groupdict()
                    parsed_id["file_format"] = "ALIGNMENT"
                    parsed_id["regex"] = self._reads_format_name_regex.get(
                        reads_format.upper()
                    )
                    return parsed_id

        if data_type == "data":
            for regex in [self._regex_by_id]:
                match = regex.match(file_id)
                if match:
                    parsed_id = match.groupdict()
                    return parsed_id

        if data_type == "variants":
            for regex in [
                self._regex_by_id,
                self._regex_by_path,
                self._regex_by_vcf_type,
            ]:
                match = regex.match(file_id)
                if match:
                    parsed_id = match.groupdict()
                    parsed_id["file_format"] = "VARIANT"
                    if "vcf_type" in parsed_id:
                        vcf_regex = self._vcf_types_regex.get(
                            parsed_id.get("opencga_study"), {}
                        ).get(parsed_id.get("vcf_type"))
                        if vcf_regex is None:
                            msg = "Pattern for file type {} not found".format(
                                parsed_id.get("vcf_type")
                            )
                            raise HtsGetHttpError(
                                reason=msg, status_code=400, error="InvalidInput"
                            )

                        parsed_id["regex"] = vcf_regex
                    return parsed_id

    @staticmethod
    def _convert_opencga_file(file_info, study):
        contigs_length = OpencgaAuthenticationManager.parse_contigs(
            file_info["attributes"]
        )

        return File(
            file_path=file_info["uri"].replace("file://", ""),
            contigs=contigs_length.keys(),
            contig_length=contigs_length,
            file_id=file_info["id"],
            response_id="study={}&fileId={}".format(study, file_info["id"]),
        )

    @staticmethod
    def parse_contigs(attributes):
        contigs = {}
        if "alignmentHeader" in attributes:
            contigs = {
                ref["sequenceName"]: ref["sequenceLength"]
                for ref in attributes["alignmentHeader"]["sequenceDiccionary"]
            }
        elif "variantFileMetadata" in attributes:
            contigs = {
                i["id"]: i["genericFields"]["length"]
                for i in attributes["variantFileMetadata"]["header"]["complexLines"]
                if i["key"] == "contig"
            }

        return contigs
