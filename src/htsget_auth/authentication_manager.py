#
# Copyright (c) 2016-2019 Genomics England Ltd.
#
# This file is part of the Genomics England implementation of the HTSGET protocol
#
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import logging
import os

from htsget_errors.htsget_http_error import HtsGetHttpError


class AuthenticationManager:
    def __init__(self, token, **kwargs):
        self.token = token
        self.options = kwargs
        self.session = self._login(self._get_session_id(self.token), self.options)

    @staticmethod
    def _login(token, options):
        raise NotImplementedError()

    @classmethod
    def login(cls, token, **kwargs):
        return cls(token=token, **kwargs)

    @staticmethod
    def _configure(options):
        raise NotImplementedError()

    def get_file(self, file_id, data_type, **kwargs):
        raise NotImplementedError()

    @staticmethod
    def _get_session_id(auth_token):
        try:
            bearer, sid = auth_token.strip().split(" ")
            assert bearer == "Bearer"
        except (ValueError, AssertionError):
            msg = "Invalid Authorization token"
            logging.error(msg)
            raise ValueError(msg)
        return sid

    def logout(self):
        self.session = None


class File:
    def __init__(self, file_path, contigs, file_id, response_id, contig_length=None):
        self.file_id = file_id
        self.contigs = contigs
        self.contig_length = contig_length
        self.file_path = file_path
        self.response_id = response_id
        if not self.isfile():
            raise HtsGetHttpError(
                reason="File does not exist on system: {}".format(self.file_path),
                status_code=404,
            )

    def __str__(self):
        return "{file_id}, {file_path}".format(
            file_id=self.file_id, file_path=self.file_path
        )

    def isfile(self):
        if os.path.isfile(self.file_path):
            return True

        logging.error("File {} is not a file".format(self.file_path))
        return False
