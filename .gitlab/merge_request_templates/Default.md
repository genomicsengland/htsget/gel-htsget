<!--
  Please ensure the PR title contains the ticket ID, short description and
  is formatted to match "KMDS-XXX - Added padding fix"

  If the PR should not be merged prefix the title WIP;
  e.g. "WIP: KMDS-XXX - Added padding fix"
  
  The PR template was adopted from CVA Frontend repo.
-->

## Overview

<!-- Describe the changes, additionally, if the PR is "WIP", explain why -->

## Related ticket

<!-- Replace XXX with the ticket ID, e.g. 123 -->
- [KMDS-XXX](https://jira.extge.co.uk/browse/KMDS-XXX)

## Checklist

<!-- Replace [ ] with [x] for the items that apply -->

- [ ] The PR only includes changes relating to 1 ticket
- [ ] I have added labels to reflect change type
- [ ] I have checked all existing and new tests pass
- [ ] I have checked that linting passes
- [ ] I have added tests to cover my changes
- [ ] I have updated the documentation where required
- [ ] I have informed the team of any impactful changes
