CODE_PATHS := handlers health htsget_auth htsget_errors htsget_server htsget_tests

GITLAB_USERNAME          ?=
GITLAB_TOKEN             ?=
CI_REGISTRY              ?= registry.gitlab.com
CI_REGISTRY_IMAGE        := ${CI_REGISTRY}/genomicsengland/htsget/gel-htsget/htsget
CI_PROJECT_NAME          ?= htsget
CI_JOB_TOKEN_READ        ?=
CI_COMMIT_REF_NAME       ?= $(shell git symbolic-ref --short -q HEAD)
CI_COMMIT_REF_NAME       := $(subst /,-,$(CI_COMMIT_REF_NAME))
VERSION                  := $(subst v,,$(CI_COMMIT_REF_NAME))
CI_COMMIT_BEFORE_SHA     := $(shell git rev-parse HEAD^1)
CI_COMMIT_SHA            ?= $(shell git rev-parse HEAD)
CI_COMMIT_SHORT_SHA      ?= $(shell git rev-parse --short HEAD)
CI_ENVIRONMENT_NAME      ?=
CI_DOCKER_BUILD_ARGS     ?=
CI_COMPOSE_ARGS			 ?= $(if ${CI},, -f docker-compose-local.yml)
COMPOSE_PROJECT_NAME     ?= ${CI_PROJECT_NAME}${CI_PIPELINE_ID}
DOCKER_IMAGE_SHA         := ${CI_REGISTRY_IMAGE}:build-${CI_COMMIT_SHA}
DOCKER_IMAGE_TAG         := ${CI_REGISTRY_IMAGE}:${VERSION}
DOCKER_IMAGE_LAYER_BASE  := ${DOCKER_IMAGE_SHA}-base
DOCKER_IMAGE_LAYER_TEST  ?= ${DOCKER_IMAGE_SHA}-test
DOCKER_IMAGE_LAYER_PROD  ?= ${DOCKER_IMAGE_SHA}-prod
DOCKER_IMAGE_LAYER_VENV  := ${DOCKER_IMAGE_SHA}-venv

DOCKER_IMAGE_BEFORE_SHA  := ${CI_REGISTRY_IMAGE}:build-${CI_COMMIT_BEFORE_SHA}
DOCKER_IMAGE_BEFORE_BASE := ${DOCKER_IMAGE_BEFORE_SHA}-base
DOCKER_IMAGE_BEFORE_TEST ?= ${DOCKER_IMAGE_BEFORE_SHA}-test
DOCKER_IMAGE_BEFORE_PROD ?= ${DOCKER_IMAGE_BEFORE_SHA}-prod
DOCKER_IMAGE_BEFORE_VENV := ${DOCKER_IMAGE_BEFORE_SHA}-venv

CI_COMPOSE_PREFIX        ?= $(if ${CI}, DOCKER_IMAGE_LAYER_TEST=${DOCKER_IMAGE_LAYER_TEST},)
PREFIX_COMPOSE           := \
	COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME} \
	${CI_COMPOSE_PREFIX} \
	docker-compose ${CI_COMPOSE_ARGS}
RUN_COMPOSE				 := ${PREFIX_COMPOSE} run --rm htsget
_DOCKER_BUILD            := docker build ${CI_DOCKER_BUILD_ARGS}

## build with cache layers
_DOCKER_BUILD__WCL       := DOCKER_BUILDKIT=0 ${_DOCKER_BUILD}

## some tests in CI can run as docker run
DOCKER_RUN_TEST			 := docker run --rm ${DOCKER_IMAGE_LAYER_TEST}
SIMPLE_RUN_TEST          := $(if ${CI}, ${DOCKER_RUN_TEST}, ${RUN_COMPOSE})

TEST_PARAMS             ?=

.PHONY: test lint isort format precommit secure compelx unused all all_unsafe

help: ## Prints this help/overview message
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-17s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

run: stop build start_foreground ## Builds all containers and (re)runs them in foreground.

restart: stop start ## Restarts all containers

build: ## Builds all containers
	${PREFIX_COMPOSE} build

rebuild: down build start ## Fully rebuild containers

start: ## Starts all containers
	${PREFIX_COMPOSE} up -d

start_foreground: ## Starts all containers in foreground
	${PREFIX_COMPOSE} up

stop: ## Stops all containers
	${PREFIX_COMPOSE} stop

down: ## Fully stops containers, removing persistence
	${PREFIX_COMPOSE} down

clean: ## Cleans all build containers and images. Stops everything as well.
	${PREFIX_COMPOSE} down --volumes --rmi all || true
	rm -rf cover .mypy_cache .venv* .coverage

status: ## Shows status of all containers
	${PREFIX_COMPOSE} ps

test: ## Updates requirements, rules and runs all available tests locally.
	${RUN_COMPOSE} pytest . ${TEST_PARAMS}

coverage: ## Runs unit test coverage test. Does not update requirements or rules.
	${RUN_COMPOSE} coverage report --skip-covered

lint: ## Runs linter on source code and tests. Does not update requirements or rules.
	${SIMPLE_RUN_TEST} pylint --rcfile=setup.cfg ${CODE_PATHS}

types: ## Runs types test. Does not update requirements or rules.
	${SIMPLE_RUN_TEST} mypy --config setup.cfg .

format:
	${SIMPLE_RUN_TEST} black . -l 120 --check

reformat:
	${SIMPLE_RUN_TEST} black . -l 120

safety:
	${SIMPLE_RUN_TEST} safety check -r requirements.txt

logs: ## Follow the logs for the microservice
	${PREFIX_COMPOSE} logs -f ${CI_PROJECT_NAME}

sh: ## Follow the logs for the microservice
	${RUN_COMPOSE} /bin/bash

# check potential security issues
secure:
	${_TEST_RUN} bandit -r $(CODE_PATHS)

# check code complexity
complex:
	${_TEST_RUN} xenon -b B -m A -a A .

# find any unused code
unused:
	${_TEST_RUN} vulture --exclude $(EXCLUDE) $(CODE_PATHS)

# below will reformat all the code
#format:
#	black $(CODE_PATHS)
#isort:
#	isort -rc $(CODE_PATHS)
#precommit: isort format

full: stop clean build test

# run all steps, stop if any fails
all: secure complex unused lint test

# will run all steps even if they fail
all_unsafe:
	sh -c "make secure; make complex; make unused; make lint; make test"

gitlab__registry__login:
	docker login \
		-u ${GITLAB_USERNAME} \
		-p ${GITLAB_TOKEN} \
		${CI_REGISTRY}

build__pull_layers: ## For CI: Pull all possible previous build/cache layers
	# --cache-from will not automatically pull - explicit pull required
	docker pull ${DOCKER_IMAGE_BEFORE_VENV} || true
	docker pull ${DOCKER_IMAGE_BEFORE_BASE} || true
	docker pull ${DOCKER_IMAGE_BEFORE_TEST} || true
	docker pull ${DOCKER_IMAGE_BEFORE_PROD} || true
	docker pull ${DOCKER_IMAGE_BEFORE_SHA}  || true

	docker pull ${DOCKER_IMAGE_LAYER_VENV}  || true
	docker pull ${DOCKER_IMAGE_LAYER_BASE}  || true
	docker pull ${DOCKER_IMAGE_LAYER_TEST}  || true
	docker pull ${DOCKER_IMAGE_LAYER_PROD}  || true
	docker pull ${DOCKER_IMAGE_SHA}         || true

build__with_cache_layers: build__pull_layers	## For CI: build dev with cache layers
	# Multistage Dockerfiles can be optimised by manually specifying layers
	# https://andrewlock.net/caching-docker-layers-on-serverless-build-hosts-with-multi-stage-builds---target,-and---cache-from/
	# https://semaphoreci.com/docs/docker/docker-layer-caching.html
	# --cache-from can currently only be used with DOCKER_BUILDKIT=0 due to a deficency in buildkit
	${_DOCKER_BUILD__WCL} \
		--target venv \
		--cache-from ${DOCKER_IMAGE_LAYER_VENV} \
		--cache-from ${DOCKER_IMAGE_BEFORE_VENV} \
		--tag        ${DOCKER_IMAGE_LAYER_VENV} \
		.
	${_DOCKER_BUILD__WCL} \
		--target base \
		--cache-from ${DOCKER_IMAGE_LAYER_BASE} \
		--cache-from ${DOCKER_IMAGE_LAYER_VENV} \
		--cache-from ${DOCKER_IMAGE_BEFORE_BASE} \
		--cache-from ${DOCKER_IMAGE_BEFORE_VENV} \
		--tag        ${DOCKER_IMAGE_LAYER_BASE} \
		.
	${_DOCKER_BUILD__WCL} \
		--target test \
		--cache-from ${DOCKER_IMAGE_LAYER_TEST} \
		--cache-from ${DOCKER_IMAGE_LAYER_BASE} \
		--cache-from ${DOCKER_IMAGE_LAYER_VENV} \
		--cache-from ${DOCKER_IMAGE_BEFORE_TEST} \
		--cache-from ${DOCKER_IMAGE_BEFORE_BASE} \
		--cache-from ${DOCKER_IMAGE_BEFORE_VENV} \
		--tag        ${DOCKER_IMAGE_LAYER_TEST} \
		.
	${_DOCKER_BUILD__WCL} \
		--target prod \
		--cache-from ${DOCKER_IMAGE_LAYER_PROD} \
		--cache-from ${DOCKER_IMAGE_LAYER_TEST} \
		--cache-from ${DOCKER_IMAGE_LAYER_BASE} \
		--cache-from ${DOCKER_IMAGE_LAYER_VENV} \
		--cache-from ${DOCKER_IMAGE_BEFORE_PROD} \
		--cache-from ${DOCKER_IMAGE_BEFORE_TEST} \
		--cache-from ${DOCKER_IMAGE_BEFORE_BASE} \
		--cache-from ${DOCKER_IMAGE_BEFORE_VENV} \
		--tag        ${DOCKER_IMAGE_LAYER_PROD} \
		.
	${_DOCKER_BUILD__WCL} \
		--cache-from ${DOCKER_IMAGE_SHA} \
		--cache-from ${DOCKER_IMAGE_LAYER_PROD} \
		--cache-from ${DOCKER_IMAGE_LAYER_TEST} \
		--cache-from ${DOCKER_IMAGE_LAYER_BASE} \
		--cache-from ${DOCKER_IMAGE_LAYER_VENV} \
		--cache-from ${DOCKER_IMAGE_BEFORE_SHA} \
		--cache-from ${DOCKER_IMAGE_BEFORE_PROD} \
		--cache-from ${DOCKER_IMAGE_BEFORE_TEST} \
		--cache-from ${DOCKER_IMAGE_BEFORE_BASE} \
		--cache-from ${DOCKER_IMAGE_BEFORE_VENV} \
		--tag        ${DOCKER_IMAGE_SHA} \
		.

build__push__tag: build__with_cache_layers
	## TODO add sem version check
	@echo "Checking version"
	[[ -z "${VERSION}" ]] && exit 1 || echo "Build tag - ${VERSION}"

	${_DOCKER_BUILD__WCL} \
		--target prod \
		--cache-from ${DOCKER_IMAGE_SHA} \
		--cache-from ${DOCKER_IMAGE_LAYER_PROD} \
		--cache-from ${DOCKER_IMAGE_LAYER_TEST} \
		--cache-from ${DOCKER_IMAGE_LAYER_BASE} \
		--cache-from ${DOCKER_IMAGE_LAYER_VENV} \
		--cache-from ${DOCKER_IMAGE_BEFORE_SHA} \
		--cache-from ${DOCKER_IMAGE_BEFORE_PROD} \
		--cache-from ${DOCKER_IMAGE_BEFORE_TEST} \
		--cache-from ${DOCKER_IMAGE_BEFORE_BASE} \
		--cache-from ${DOCKER_IMAGE_BEFORE_VENV} \
		--build-arg HTSGET_VERSION=${VERSION} \
		--build-arg HTSGET_COMMIT=${CI_COMMIT_SHA} \
		--build-arg HTSGET_COMMIT_SHORT=${CI_COMMIT_SHORT_SHA} \
		--tag ${DOCKER_IMAGE_TAG} \
		.
	docker push ${DOCKER_IMAGE_TAG}

build__push_layers:	## For CI: push all generated cache layers
	docker push ${DOCKER_IMAGE_LAYER_VENV}
	docker push ${DOCKER_IMAGE_LAYER_BASE}
	docker push ${DOCKER_IMAGE_LAYER_TEST}
	docker push ${DOCKER_IMAGE_LAYER_PROD}
	docker push ${DOCKER_IMAGE_SHA}

## Delete local layers as gitlab runs two pipelines for merge requests and sometimes the
## hashes don't match, resulting in rebuilding the image which takes time
build__delete_local_layers:
	docker rmi -f ${DOCKER_IMAGE_LAYER_VENV}
	docker rmi -f ${DOCKER_IMAGE_LAYER_BASE}
	docker rmi -f ${DOCKER_IMAGE_LAYER_TEST}
	docker rmi -f ${DOCKER_IMAGE_LAYER_PROD}
	docker rmi -f ${DOCKER_IMAGE_SHA}
	docker rmi -f ${DOCKER_IMAGE_BEFORE_VENV}
	docker rmi -f ${DOCKER_IMAGE_BEFORE_BASE}
	docker rmi -f ${DOCKER_IMAGE_BEFORE_TEST}
	docker rmi -f ${DOCKER_IMAGE_BEFORE_PROD}
	docker rmi -f ${DOCKER_IMAGE_BEFORE_SHA}
