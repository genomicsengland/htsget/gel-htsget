ARG DOCKER_IMAGE_BASE=python:3.9.21-slim

# Deal with non-application installs - this was done twice, so was prone to cache invalidation
FROM ${DOCKER_IMAGE_BASE} as sys_base
RUN apt-get update && apt-get upgrade -y && apt-get install --no-install-recommends -y \
    bcftools \
    net-tools \
    procps \
    supervisor \
    wget \
    bzip2 \
    gcc \
    make \
    libbz2-dev \
    zlib1g-dev \
    libncurses5-dev \
    libncursesw5-dev \
    liblzma-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Upgrade system-level pip and setuptools
RUN wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py setuptools && rm get-pip.py

# Download, build, install, and clean up samtools 1.9. 
# Note that version 1.9 works and updating can cause issues loading CRAMS in IGV
RUN wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 && \
    tar -xjf samtools-1.9.tar.bz2 && \
    cd samtools-1.9 && \
    ./configure --prefix=/usr/local && \
    make && \
    make install && \
    cd .. && \
    rm -rf samtools-1.9 samtools-1.9.tar.bz2

# Verify installation
RUN samtools --version

# Virtualenv setup ------------------------------------------------------------
FROM sys_base as venv

RUN pip install --no-cache-dir \
    setuptools \
    virtualenv \
    && true

RUN virtualenv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# Main container --------------------------------------------------------------
FROM sys_base as base

# Copy python packages
COPY --from=venv /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN pip install typing_extensions

# /app is hard coded in config - let's assume this location is constant
ARG WORKDIR=/app
ENV WORKDIR=${WORKDIR}
WORKDIR ${WORKDIR}

# Setup folders and settings config
RUN mkdir htsget_server && \
    mkdir -p log/supervisord && \
    mkdir run && \
    mkdir conf && \
    mkdir cicd && \
    touch conf/settings.ini

COPY ./supervisord.conf /etc/supervisor/supervisord.conf
COPY ./cicd ${WORKDIR}/cicd

RUN ${WORKDIR}/cicd/get_version.sh "${WORKDIR}/cicd/app_versions.tfvars.json" "${WORKDIR}/cicd/htsget_version"

COPY pyproject.toml ${WORKDIR}
RUN pip install poetry==1.6.1
RUN poetry config virtualenvs.create false
RUN poetry version "$(cat ${WORKDIR}/cicd/htsget_version)"

FROM base as test
COPY src ${WORKDIR}
ENV PYTHONPATH  "$PYTHONPATH:${WORKDIR}"

RUN poetry install --with dev

FROM base as prod
COPY src ${WORKDIR}

ARG HTSGET_VERSION="0.0.0"
ARG HTSGET_COMMIT="zzz"
ARG HTSGET_COMMIT_SHORT="zzz"

ENV HTSGET_VERSION=${HTSGET_VERSION}
ENV HTSGET_COMMIT=${HTSGET_COMMIT}
ENV HTSGET_COMMIT_SHORT=${HTSGET_COMMIT_SHORT}

ENV DD_SERVICE="htsget"
ENV DD_VERSION=${HTSGET_VERSION}

ENV SETTINGS_LOCATION=/app/conf/settings.ini
ENV PYTHONPATH  "$PYTHONPATH:${WORKDIR}"

RUN poetry install --without dev

EXPOSE 8000 8001 8002 8003 8004
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf", "-n"]
